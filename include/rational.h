#ifndef _RATIONAL_H_
#define _RATIONAL_H_

#include "include/big_int.h"

class Rational {

    public:
        Rational(std::string);
        Rational(BigInt, BigInt);
        ~Rational();

        // copy ctor
        Rational(const Rational &);

        // assignment op
        Rational & operator = (const Rational &);

        // arithmetic
        friend Rational operator + (const Rational &, const Rational &);
        friend Rational operator / (const Rational &, const Rational &);


        // geters
        BigInt GetAbove() const;
        BigInt GetBelow() const;

    private:
        Rational() {}

        BigInt * mp_above;
        BigInt * mp_below;
};

#endif//_RATIONAL_H_
