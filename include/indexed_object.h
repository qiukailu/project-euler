#ifndef _INDEXED_OBJECT_H_
#define _INDEXED_OBJECT_H_

class indexedObject {
    public:
        indexedObject() {
            m_i = m_j = m_k = 0;
        }

        indexedObject(int i, int j, int k) {
            m_i = i;
            m_j = j;
            m_k = k;
        }

        void seti(int i) { m_i = i; }
        void setj(int j) { m_j = j; }
        void setk(int k) { m_k = k; }

	int i() { return m_i; }
	int j() { return m_j; }
	int k() { return m_k; }

    private:
        int m_i;
        int m_j;
        int m_k;
};

#endif//_INDEXED_OBJECT_H_
