/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef _MYINT_H_
#define _MYINT_H_

#include <iostream>
#include <vector>
#include <map>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>

#include "indexed_object.h"

/// extended integer class
class myInt : public indexedObject {
  public:

    myInt ();

    myInt(uint64_t in);

    ~myInt ();
    
    void fill_in_primes(int n);

    void setValue(uint64_t in);

    uint64_t value();

    /// number of divisors, excluding 1 and itself
    int num_divisors();

    /// sum of proper divisors, defined as numbers less than n which divide evenly into n
    int sum_proper_divs();

    /// this is the sum of all divisors including itself
    int sum_of_divisors();

    /// calculate perfect number, returns < 0 for deficient, == 0 for perfect, and > 0 for abundant
    int is_perfect();

    // check if number is pandigital.
    bool is_pandigital();

    static bool is_prime_num(uint64_t inum);

    bool is_prime();

    bool is_divisor(int value);

    void calc_prime_divisors();

    std::vector<int> get_prime_divisors();

    uint64_t reverse_itself();

    std::string unit_fraction(int precision = 256);

    // put all digits in the returned vector, e.g., 123 => 1,2,3
    std::vector<int> get_digits();

    // pretty obvious what this does
    int get_num_digits();

    uint64_t power_sum(int exp);

    std::map<int, std::vector<int>> get_repeating_digits_map();

  private:

    uint64_t m_data;
    std::vector<int> m_prime_vec;
    std::vector<int> m_prime_divisors;
    std::vector<int> m_prime_exp;

    bool m_prime_divisors_calculated;
};

typedef class myInt * pSuperInt;

#endif // _MYINT_H_
