#ifndef POKER_GAME_H
#define POKER_GAME_H

#include <string>
#include <vector>

namespace Poker {

    class Card;
    class Hand;

    class Game {

      public:

        Game(std::string deck);
        ~Game();

        int CompareRanks();
        bool Player1Wins();

      private:
        std::vector <Card * > m_cards;
        Hand * m_player1;
        Hand * m_player2;
        
    };
} // namespace Poker


#endif//POKER_GAME_H
