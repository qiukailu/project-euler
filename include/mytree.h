/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//
//  mytree.h
//  
//
//  Created by Qiukai Lu on 8/4/16.
//
//

#ifndef mytree_h
#define mytree_h

class leaf {
private:
    leaf * m_left;
    leaf * m_right;
    int m_value;
    
    // disable default ctor
    leaf();
    
public:
    leaf(int i);
    leaf(int i, leaf** left, leaf** right);
    
    void setLeft(leaf ** p_leaf);
    void setRight(leaf ** p_leaf);
    void setValue(int i_value);
    
    leaf* getLeft();
    leaf* getRight();
    int   getValue();

    int   max_sum();
};

#endif /* mytree_h */
