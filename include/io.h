#include <string>
#include <iostream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <assert.h>

class ImportRawDataFrom {

    public:

        // ctor -- with filename as input
        ImportRawDataFrom(std::string filename) {
            
            m_data_stream.open(filename, std::ifstream::in);

            assert(m_data_stream.is_open() == true);
//            if (m_data_stream.is_open() == false) {
//                std::cout << "Error in opening file " << filename << std::endl;
//            }
        }

        ~ImportRawDataFrom() {

            // close the file
            m_data_stream.close();

        }

        // =============================================================================
        std::vector<std::string> GetStringsDividedBy(std::string tokens) {

            using namespace boost::algorithm;

            std::string line;
            std::vector<std::string> retval;
            // ---- data processing ----
            while ( getline (m_data_stream, line) )
            {
                split(retval, line, is_any_of(tokens), token_compress_on );
            }

            return retval;

        }


    private:
        // disable default ctor
        ImportRawDataFrom() {}

        std::ifstream m_data_stream;
};
