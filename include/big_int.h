#ifndef _BIG_INT_H_
#define _BIG_INT_H_

#include <string>

#include <gmpxx.h>

// this is an arbitrary precision integer class built around gmp library
class BigInt {

    public:

        BigInt(signed long int);
        BigInt(int);
        BigInt(mpz_t);
        BigInt(std::string);
        ~BigInt();

        // copy ctor
        BigInt(const BigInt &);

        // assignment operator
        BigInt & operator=(const BigInt &);

        // comparison operators
        bool operator == (const BigInt &);
        bool operator != (const BigInt &);
        bool operator >  (const BigInt &);
        bool operator >= (const BigInt &);
        bool operator <  (const BigInt &);
        bool operator <= (const BigInt &);

        // arithmetic operators
        friend BigInt operator + (const BigInt &, const BigInt &);
        friend BigInt operator - (const BigInt &, const BigInt &);
        friend BigInt operator * (const BigInt &, const BigInt &);
        friend BigInt operator / (const BigInt &, const BigInt &);
        friend BigInt operator % (const BigInt &, const BigInt &);
        friend BigInt operator ^ (const BigInt &, const unsigned long int);

        // get values
        std::string GetValueAsStr();

        // fancy operations
        BigInt Reverse();
        bool IsPalindromic();
        bool IsLychrel(unsigned int &); // default max iter == 50
        bool IsLychrel(unsigned int, unsigned int &);
        unsigned int DigitSum();
        unsigned int NumDigits();

        /// concatenate two BigInt objects by appending b at the end
        BigInt ConcatWith(BigInt & b);

    private:
        BigInt() {}

        void _Initialize();

        // data member
        mpz_t m_data;

};

#endif//_BIG_INT_H_
