#ifndef POKER_CARD_H
#define POKER_CARD_H

#include <string>
#include <vector>

namespace Poker {

    class Card {

      public:
        Card();
        Card(std::string);
        ~Card();
        
        void SetValue(std::string);
        int  GetValueAsInt();
        char GetValueAsChar();
        char GetSuit();

      private:
        char m_suit;
        char m_value;
    };
}

#endif//POKER_CARD_H
