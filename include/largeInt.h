/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef LARGEINT_H
#define LARGEINT_H

#include<vector>
#include<iostream>
#include<algorithm>
#include<cmath>

template <class T>
class largeInt {

  public:
    largeInt() {

    }

    static bool is_perfect_square(T a) {

        if (a < 1) return false;

        double a_sqrt = std::sqrt(a);
        T a_floor = std::floor(a_sqrt);
        T a_ceil  = std::ceil(a_sqrt);

        return ( ( a_floor * a_floor == a ) || ( a_ceil * a_ceil == a ) );
    }

    // turn 12, 34, 567 into 1234567
    static T concate_two_ints(T a, T b) {


        T retval = 0;

        T base = 10;

        while( b / base > 0 ) {
            base = base * 10;
        }

        retval = a * base + b;

        return retval;
    }


    static T get_value_from_digits(std::vector<int> digits) {
        T retval = 0;
        for(int i = 0; i < digits.size(); ++i) {
            retval = retval * 10 + digits[i];
        }

        return retval;
    }

    // convert decimal integer to binary form
    static std::vector<int> decimal_to_binary_digits(T dec_num) {


        std::vector<int> bin_digits;

        while (dec_num != 0) {
            int remainder = dec_num % 2;
            dec_num /= 2;
            bin_digits.push_back(remainder);
        }

        std::reverse(std::begin(bin_digits), std::end(bin_digits));

        return bin_digits;
    }

    /// get the greatest common divisor of the given two integers
    static T get_gcd(T a, T b) {

        if (a < b) {
            std::swap(a, b);
        }

        if (b == 0) return a;

        return largeInt::get_gcd(b, a%b);

    }

    /// check if a given value is a prime number
    static bool is_prime_num(T ivalue) {
        if (ivalue <= 1) {

            return false;

        } else if (ivalue <= 3) {

            return true;

        } else if (ivalue % 2 == 0 || ivalue % 3 == 0) {

            return false;

        } else {

            T divider = 5;

            while ( divider * divider <= ivalue ) {
                if ( ivalue % divider == 0 || ivalue % (divider + 2) == 0 ) {
                    return false;
                }

                divider = divider + 6;
            }

        }

        return true;
    }

    static bool is_triangle_number(T test_num) {

        if (test_num <= 0)
            return false;

        double data = test_num * 2.0;

        double sqrt_data = std::sqrt(data);

        T l = std::floor(sqrt_data);

        return ( l * (l + 1) == test_num * 2 );
    }

    static bool is_pentagon_number(T test_num) {

        if (test_num <= 0)
            return false;

        double data = test_num * 6.0;

        double sqrt_data = std::sqrt(data);

        T c = std::ceil(sqrt_data);

        // 3n * (3n - 1) => floor + 1 has to be modulo 3
        if ( c % 3 != 0 ) return false;

        return ( c * (c - 1) == test_num * 6 );
    }

    static bool is_hexagon_number(T test_num) {

        if (test_num <= 0)
            return false;

        double data =  test_num * 2.0;

        double sqrt_data = std::sqrt(data);

        T c = std::ceil(sqrt_data);

        if ( c % 2 != 0 )  return false;

        return ( c * (c - 1) == test_num * 2 );
    }

    static bool is_pentagonal_pair(T a, T b) {
        T sum = a + b;
        T dif = b - a;

        return (is_pentagon_number(sum) && is_pentagon_number(dif));
    }


    static T gen_penta_number(int index) {
        T retval = index * (3 * index - 1) / 2;
        return retval;
    }

    static std::vector<T> get_all_divisors(T ivalue) {
        std::vector<T> divisors;
	divisors.clear();
	for (T i = 1; i*i <= ivalue; ++i) {
	    if (ivalue % i == 0) {
                divisors.push_back(i);
		if ( i * i != ivalue ) {
		    divisors.push_back(ivalue/i);
		}
	    }
	}
        std::sort(divisors.begin(), divisors.end());
        return divisors;
    }

    /// calculate factorial
    static T factorial(T n) {
        return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
    }

    /// calculate combinatorics
    static T combinatory(T r, T n) {
        T fac_n = factorial(n);
        T fac_r = factorial(r);
        T fac_r_n = factorial(n-r);

        fac_r = fac_r * fac_r_n;
        T result = fac_n / fac_r;

        return result;
    }

    static T get_index(T total, T n) {
        T i = 0;
        while (1) {
            if ( i * n < total && (i + 1) * n > total ) {
                return i;
            }
            if ( i * n == total )
                return i;
            i++;
        }
    }

    static T compute_diag(T dim) {
        if (dim <= 1) return 1;

        T distance = dim - 1;
        T a = dim * dim;
        T b = a - distance;
        T c = b - distance;
        T d = c - distance;
        return a + b + c + d + compute_diag(dim - 2);

    }
};

#endif // LARGEINT_H
