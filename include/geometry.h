/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef GEOMETRY_H
#define GEOMETRY_H

#include<vector>
#include<iostream>
#include<algorithm>

template <class T>
class geometry {

  public:
    geometry() {

    }

    // check if three integers form a valid triangle
    static bool is_valid_triangle(T a, T b, T c) {
        if (a + b > c) {
            return true;
        } else {
            return false;
        }
    }

    // check if three integers form a right angle triangle
    static bool is_right_angle_triangle(T a, T b, T c) {
        if ( geometry<T>::is_valid_triangle(a, b, c) == false ) {
            return false;
        }

        if ( a * a + b * b == c * c ) {
            return true;
        } else {
            return false;
        }
    }

};

#endif // GEOMETRY_H
