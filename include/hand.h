#ifndef POKER_HAND_H
#define POKER_HAND_H

#include <string>
#include <vector>
#include <assert.h>

#include <boost/algorithm/string.hpp>

namespace Poker {

    enum Rank {PLAIN, ONE_PAIR, TWO_PAIRS, THREE_OF_A_KIND, STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH, ROYAL_FLUSH};

    class Card;

    class Hand {

      public:
        Hand();
        ~Hand();

        void AssignCard(Card * pcard);
      
        int GetNumberOfCardsByValue(char cvalue);

        int GetNumberOfCardsBySuit(char csuit);

        // check ranks
        bool IsRoyalFlush();
        bool IsStraightFlush();
        bool IsFourOfAKind();
        bool IsFullHouse();
        bool IsFlush();
        bool IsStraight();
        bool IsThreeOfAKind();
        bool HasTwoPairs();
        bool HasOnePair();
        int  GetHighestValueAsInt();

        //
        int  GetRank();
        std::vector<int> GetWeightedCardValues();

      private:
        
        bool _AllInSameSuit();
        bool _ConsecutiveCards();
        int  _GetNumberOfPairs(std::vector<char> & pair_values);

        std::vector<Card *> m_p_cards;
        std::vector<int> m_weighted_card_values;
    };

} // namespace Poker


#endif//POKER_HAND_H
