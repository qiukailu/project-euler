#!/bin/bash -ex

export CURRENT_SOURCE_DIR=${PWD};
export GTEST_BUILD_DIR=${PWD}/external/googletest/cmake_build;
export GTEST_BUILD_INSTALL_DIR=${PWD}/external/googletest-install;

rm -rf ${GTEST_BUILD_DIR};
rm -rf ${GTEST_BUILD_INSTALL_DIR};

mkdir -p ${GTEST_BUILD_DIR};
cd ${GTEST_BUILD_DIR};
cmake ../ -DCMAKE_INSTALL_PREFIX=${GTEST_BUILD_INSTALL_DIR};
make install;
