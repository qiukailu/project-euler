/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <fstream>
#include <gtest/gtest.h>
#include "include/largeInt.h"
#include "include/geometry.h"
#include "include/myInteger.h"
#include "include/card.h"
#include "include/hand.h"
#include "include/game.h"
#include "include/big_int.h"
#include "include/rational.h"

TEST (rational, addition) {
    Rational a("1/2");
    Rational b("1/3");
    Rational c = a + b;
    Rational d(c);

    BigInt ca = c.GetAbove();
    BigInt cb = c.GetBelow();

    EXPECT_TRUE(ca == 5);
    EXPECT_TRUE(cb == 6);

    EXPECT_TRUE(d.GetAbove() == 5);
    EXPECT_TRUE(d.GetBelow() == 6);

    // ========
    //
    a = Rational("5");
    b = Rational("1/5");
    c = a + b;
    ca = c.GetAbove();
    cb = c.GetBelow();

    EXPECT_TRUE(ca == 26);
    EXPECT_TRUE(cb == 5);
}

TEST (rational, division) {

    Rational a("1/2");
    Rational b("1/3");
    Rational c = a / b;

    BigInt ca = c.GetAbove();
    BigInt cb = c.GetBelow();

    EXPECT_TRUE(ca == 3);
    EXPECT_TRUE(cb == 2);

    Rational d("1");
    c = d / c;

    ca = c.GetAbove();
    cb = c.GetBelow();

    EXPECT_TRUE(ca == 2);
    EXPECT_TRUE(cb == 3);

}

TEST (big_int, num_digits) {
    BigInt a = 12345670089;
    EXPECT_TRUE(a.NumDigits() == 11);

}

TEST (big_int, concatenate) {
    BigInt a = 123;
    BigInt b = 456;
    
    BigInt c = a.ConcatWith(b);
    EXPECT_TRUE(c == 123456);

    BigInt d = b.ConcatWith(a);
    EXPECT_TRUE(d == 456123);
}

TEST (big_int, modulo) {
    BigInt a = 123;
    BigInt b = 10;
    BigInt c = a % b;
    EXPECT_TRUE(c == 3);
}

TEST (big_int, reverse) {

    BigInt a = 47;
    BigInt b = a.Reverse();

    BigInt c = a + b;
    EXPECT_TRUE(c == 121);

}

TEST (big_int, is_palindromic) {
    BigInt a = 123456789;
    EXPECT_FALSE (a.IsPalindromic());

    BigInt b = 123454321;
    EXPECT_TRUE  (b.IsPalindromic());
}

TEST (big_int, comparison_operators) {
    BigInt a(54321);
    BigInt b(12345);
    BigInt c = 54321;

    EXPECT_TRUE  (a != b);
    EXPECT_TRUE  (a > b);
    EXPECT_TRUE  (a >= b);
    EXPECT_FALSE (a == b);
    EXPECT_FALSE (a < b);
    EXPECT_FALSE (a <= b);

    EXPECT_TRUE  (a == c);
    EXPECT_TRUE  (a >= c);
    EXPECT_TRUE  (a <= c);
    EXPECT_FALSE (a != c);
    EXPECT_FALSE (a > c);
    EXPECT_FALSE (a < c);

    EXPECT_TRUE (c == 54321);
    EXPECT_FALSE(c != 54321);
    EXPECT_TRUE (c != 64321);
    EXPECT_FALSE(c == 64321);
}

TEST (big_int, assignment) {
    BigInt a(67890);
    BigInt b(11111);
    b = a;

    EXPECT_TRUE(b == a);
}

TEST (big_int, copy_ctor) {
    BigInt a(67890);
    BigInt c = a;

    EXPECT_TRUE(c == a);
}

TEST (big_int, arithmetic) {
    BigInt a = 1234567;
    BigInt b = 7654321;
    BigInt c = a + b;
    BigInt d = 8888888;

    EXPECT_TRUE(c == d);
    
    BigInt aa = 1234;
    aa = aa + 1;
    EXPECT_TRUE(aa == 1235);

    aa = aa - 10;
    EXPECT_TRUE(aa == 1225);

    BigInt e = 12;
    BigInt f = 13;
    BigInt g = e * f;
    BigInt h = 156;
    EXPECT_TRUE(g == h);

    BigInt n = 361;
    n = n / 9;
    EXPECT_TRUE(n == 40);
}

TEST (game_tests, compare_ranks) {

    Poker::Game g1("5H 5C 6S 7S KD 2C 3S 8S 8D TD");
    EXPECT_EQ(g1.CompareRanks(), 0);
    EXPECT_FALSE(g1.Player1Wins());

    Poker::Game g2("5D 8C 9S JS AC 2C 5C 7D 8S QH");
    EXPECT_EQ(g2.CompareRanks(), 0);
    EXPECT_TRUE (g2.Player1Wins());

    Poker::Game g3("2D 9C AS AH AC 3D 6D 7D TD QD");
    EXPECT_EQ(g3.CompareRanks(), -1);
    EXPECT_FALSE(g3.Player1Wins());

    Poker::Game g4("4D 6S 9H QH QC 3D 6D 7H QD QS");
    EXPECT_EQ(g4.CompareRanks(), 0);
    EXPECT_TRUE (g4.Player1Wins());

    Poker::Game g5("2H 2D 4C 4D 4S 3C 3D 3S 9S 9D");
    EXPECT_EQ(g5.CompareRanks(), 0);
    EXPECT_TRUE (g5.Player1Wins());
    
}

TEST (card_tests, get_values)
{
    Poker::Hand h;
    Poker::Card c;
    c.SetValue("8H");
    int ivalue = c.GetValueAsInt();
    char cvalue = c.GetValueAsChar();

    EXPECT_EQ(ivalue, 8);
    EXPECT_EQ(cvalue, '8');
}

TEST (hand_tests, get_number_of_cards_by_value)
{
    Poker::Card c0("5H");
    Poker::Card c1("5C");
    Poker::Card c2("6S");
    Poker::Card c3("7S");
    Poker::Card c4("KD");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_EQ(my_hand.GetNumberOfCardsByValue('5'), 2);
    EXPECT_EQ(my_hand.GetNumberOfCardsBySuit('S'), 2);
}

TEST (hand_tests, royal_flush) {

    Poker::Card c0("TH");
    Poker::Card c1("AH");
    Poker::Card c2("QH");
    Poker::Card c3("KH");
    Poker::Card c4("JH");
    Poker::Card c5("9H");
    Poker::Card c6("JS");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::ROYAL_FLUSH);

    Poker::Hand my_hand2;    
    my_hand2.AssignCard(&c0);
    my_hand2.AssignCard(&c1);
    my_hand2.AssignCard(&c2);
    my_hand2.AssignCard(&c3);
    my_hand2.AssignCard(&c5);
    EXPECT_FALSE(my_hand2.IsRoyalFlush());

    Poker::Hand my_hand3;    
    my_hand3.AssignCard(&c0);
    my_hand3.AssignCard(&c1);
    my_hand3.AssignCard(&c2);
    my_hand3.AssignCard(&c3);
    my_hand3.AssignCard(&c6);
    EXPECT_FALSE(my_hand3.IsRoyalFlush());

}


TEST (hand_tests, straight_flush) {
    Poker::Card c0("8S");
    Poker::Card c1("7S");
    Poker::Card c2("9S");
    Poker::Card c3("JS");
    Poker::Card c4("TS");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::STRAIGHT_FLUSH);
}

TEST (hand_tests, is_four_of_a_kind) {
    Poker::Card c0("8S");
    Poker::Card c1("KS");
    Poker::Card c2("8S");
    Poker::Card c3("8S");
    Poker::Card c4("8S");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::FOUR_OF_A_KIND);
}

TEST (hand_tests, is_full_house) {
    Poker::Card c0("7S");
    Poker::Card c1("AD");
    Poker::Card c2("7C");
    Poker::Card c3("AS");
    Poker::Card c4("7H");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::FULL_HOUSE);
}

TEST (hand_tests, is_flush) {
    Poker::Card c0("7S");
    Poker::Card c1("AS");
    Poker::Card c2("7S");
    Poker::Card c3("3S");
    Poker::Card c4("7S");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::FLUSH);
 
}

TEST (hand_tests, is_straight) {
    Poker::Card c0("7S");
    Poker::Card c1("6S");
    Poker::Card c2("8S");
    Poker::Card c3("TS");
    Poker::Card c4("9H");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.IsStraight());
    EXPECT_FALSE(my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::STRAIGHT);
 
}

TEST (hand_tests, is_three_of_a_kind) {
    Poker::Card c0("TS");
    Poker::Card c1("TS");
    Poker::Card c2("8S");
    Poker::Card c3("TS");
    Poker::Card c4("9H");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.IsThreeOfAKind());
    EXPECT_FALSE(my_hand.IsStraight());
    EXPECT_FALSE(my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::THREE_OF_A_KIND);
 
}

TEST (hand_tests, has_two_pairs) {
    Poker::Card c0("TS");
    Poker::Card c1("TS");
    Poker::Card c2("8S");
    Poker::Card c3("9S");
    Poker::Card c4("9H");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.HasTwoPairs());
    EXPECT_FALSE(my_hand.IsThreeOfAKind());
    EXPECT_FALSE(my_hand.IsStraight());
    EXPECT_FALSE(my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::TWO_PAIRS);
 
}

TEST (hand_tests, has_one_pair) {
    Poker::Card c0("TS");
    Poker::Card c1("TS");
    Poker::Card c2("8S");
    Poker::Card c3("9S");
    Poker::Card c4("AH");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_TRUE (my_hand.HasOnePair());
    EXPECT_FALSE(my_hand.HasTwoPairs());
    EXPECT_FALSE(my_hand.IsThreeOfAKind());
    EXPECT_FALSE(my_hand.IsStraight());
    EXPECT_FALSE(my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::ONE_PAIR);
 
}

TEST (hand_tests, get_highest_value_as_int)
{
    Poker::Card c0("5D");
    Poker::Card c1("8C");
    Poker::Card c2("9S");
    Poker::Card c3("JS");
    Poker::Card c4("AC");

    Poker::Hand my_hand;
    my_hand.AssignCard(&c0);
    my_hand.AssignCard(&c1);
    my_hand.AssignCard(&c2);
    my_hand.AssignCard(&c3);
    my_hand.AssignCard(&c4);

    EXPECT_EQ(my_hand.GetHighestValueAsInt(), 14);
    EXPECT_FALSE(my_hand.HasOnePair());
    EXPECT_FALSE(my_hand.HasTwoPairs());
    EXPECT_FALSE(my_hand.IsThreeOfAKind());
    EXPECT_FALSE(my_hand.IsStraight());
    EXPECT_FALSE(my_hand.IsFlush());
    EXPECT_FALSE(my_hand.IsFullHouse());
    EXPECT_FALSE(my_hand.IsFourOfAKind());
    EXPECT_FALSE(my_hand.IsStraightFlush());
    EXPECT_FALSE(my_hand.IsRoyalFlush());
    EXPECT_EQ(my_hand.GetRank(), Poker::PLAIN);
}

TEST (large_int_tests, get_gcd)
{
    int igcd = largeInt<int>::get_gcd(54, 24);
    EXPECT_EQ(igcd, 6);

    BigInt bigcd = largeInt<BigInt>::get_gcd(54, 24);
    EXPECT_TRUE(bigcd == 6);
}

TEST (large_int_tests, is_perfect_square)
{
    // int type
    int test_val = -1;
    EXPECT_EQ(false, largeInt<int>::is_perfect_square(test_val));

    // int type
    test_val = 1;
    EXPECT_EQ(true, largeInt<int>::is_perfect_square(test_val));

    test_val = 8;
    EXPECT_EQ(false, largeInt<int>::is_perfect_square(test_val));

    test_val = 16;
    EXPECT_EQ(true, largeInt<int>::is_perfect_square(test_val));

    // long int type
    uint64_t long_test_val = 152415765279684;
    EXPECT_EQ(true, largeInt<uint64_t>::is_perfect_square(long_test_val));

    long_test_val = 152415765279683;
    EXPECT_EQ(false, largeInt<uint64_t>::is_perfect_square(long_test_val));

}

TEST (large_int_tests, decimal_to_binary_digits)
{

    std::vector<int> digits = largeInt<int>::decimal_to_binary_digits(100);
    std::vector<int> expected({1,1,0,0,1,0,0});

    EXPECT_EQ(true, digits == expected);

    std::vector<int> more_digits = largeInt<int>::decimal_to_binary_digits(1000000);
    std::vector<int> more_expected({1,1,1,1,0,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0});

    EXPECT_EQ(true, more_digits == more_expected);
}

TEST (large_int_tests, get_value_from_digits) {
    std::vector<int> digits({0,9});
    uint64_t expected = 9;

    uint64_t value = largeInt<uint64_t>::get_value_from_digits(digits);

    EXPECT_EQ(value, expected);
}

TEST (large_int_tests, concate_two_ints) {
    uint64_t a = 54321;
    uint64_t b = 100;
    uint64_t c = 0;

    uint64_t value    = largeInt<uint64_t>::concate_two_ints(a, c);
    uint64_t expected = 543210;

    EXPECT_EQ(value, expected);

    value    = largeInt<uint64_t>::concate_two_ints(c, b);
    expected = 100;

    EXPECT_EQ(value, expected);

    value    = largeInt<uint64_t>::concate_two_ints(a, b);
    expected = 54321100;

    EXPECT_EQ(value, expected);

}

TEST (large_int_tests, is_triangle_number) {

    EXPECT_FALSE(largeInt<uint64_t>::is_triangle_number(0));

    EXPECT_TRUE(largeInt<uint64_t>::is_triangle_number(40755));

}

TEST (large_int_tests, is_pentagon_number) {

    EXPECT_FALSE(largeInt<uint64_t>::is_pentagon_number(48));
    EXPECT_FALSE(largeInt<uint64_t>::is_pentagon_number(442));
    EXPECT_FALSE(largeInt<uint64_t>::is_pentagon_number(0));

    EXPECT_TRUE(largeInt<uint64_t>::is_pentagon_number(40755));
    EXPECT_TRUE(largeInt<uint64_t>::is_pentagon_number(92));

}

TEST (large_int_tests, is_hexagon_number) {

    EXPECT_FALSE(largeInt<uint64_t>::is_hexagon_number(0));

    EXPECT_TRUE(largeInt<uint64_t>::is_hexagon_number(40755));

}

TEST (geometry_tests, is_valid_triangle) {
    
    EXPECT_TRUE(geometry<int>::is_valid_triangle(3, 4, 5));
    EXPECT_FALSE(geometry<int>::is_valid_triangle(10, 20, 30));

}

TEST (geometry_tests, is_right_angle_triangle) {
    
    EXPECT_TRUE (geometry<int>::is_right_angle_triangle(3, 4, 5));
    EXPECT_FALSE(geometry<int>::is_right_angle_triangle(10, 20, 30));
    EXPECT_FALSE(geometry<int>::is_right_angle_triangle(30, 35, 30));

}

TEST (my_integer_tests, get_repeating_digits_map) {
    std::map<int, std::vector<int>> repeating_digits_map = myInt(121313).get_repeating_digits_map();
    std::vector<int> original_digits = myInt(121313).get_digits();

    EXPECT_EQ(repeating_digits_map[0].size(), 0);
    EXPECT_EQ(repeating_digits_map[1].size(), 3);
    EXPECT_EQ(repeating_digits_map[2].size(), 1);
    EXPECT_EQ(repeating_digits_map[3].size(), 2);
    
    EXPECT_EQ(repeating_digits_map[1][0], 0);
    EXPECT_EQ(repeating_digits_map[1][1], 2);
    EXPECT_EQ(repeating_digits_map[1][2], 4);

    std::vector<int> indices = repeating_digits_map[3];
    std::vector<int> new_digits = original_digits;
    for (int l = 0; l < indices.size(); l++) {
        new_digits[indices[l]] = 9;
    }

    int64_t value = largeInt<uint64_t>::get_value_from_digits(new_digits);
    EXPECT_EQ(value, 121919);

    // replace first and third "1"
    EXPECT_TRUE(myInt::is_prime_num(121313));
    EXPECT_FALSE(myInt::is_prime_num(222313));
    EXPECT_FALSE(myInt::is_prime_num(323313));
    EXPECT_TRUE(myInt::is_prime_num(424313));
    EXPECT_TRUE(myInt::is_prime_num(525313));
    EXPECT_FALSE(myInt::is_prime_num(626313));
    EXPECT_TRUE(myInt::is_prime_num(727313));
    EXPECT_FALSE(myInt::is_prime_num(828313));
    EXPECT_FALSE(myInt::is_prime_num(929313));
    // replace first and fifth "1"
    EXPECT_TRUE(myInt::is_prime_num(121313));
    EXPECT_FALSE(myInt::is_prime_num(221323));
    EXPECT_FALSE(myInt::is_prime_num(321333));
    EXPECT_FALSE(myInt::is_prime_num(421343));
    EXPECT_FALSE(myInt::is_prime_num(521353));
    EXPECT_FALSE(myInt::is_prime_num(621363));
    EXPECT_FALSE(myInt::is_prime_num(721373));
    EXPECT_TRUE(myInt::is_prime_num(821383));
    EXPECT_FALSE(myInt::is_prime_num(921393));
    // replace third and fifth "1"
    EXPECT_TRUE(myInt::is_prime_num(121313));
    EXPECT_TRUE(myInt::is_prime_num(122323));
    EXPECT_FALSE(myInt::is_prime_num(123333));
    EXPECT_TRUE(myInt::is_prime_num(124343));
    EXPECT_TRUE(myInt::is_prime_num(125353));
    EXPECT_FALSE(myInt::is_prime_num(126363));
    EXPECT_TRUE(myInt::is_prime_num(127373));
    EXPECT_FALSE(myInt::is_prime_num(128383));
    EXPECT_FALSE(myInt::is_prime_num(129393));
    // replace first, third and fifth "1"
    EXPECT_TRUE(myInt::is_prime_num(121313));
    EXPECT_TRUE(myInt::is_prime_num(222323));
    EXPECT_TRUE(myInt::is_prime_num(323333));
    EXPECT_TRUE(myInt::is_prime_num(424343));
    EXPECT_TRUE(myInt::is_prime_num(525353));
    EXPECT_TRUE(myInt::is_prime_num(626363));
    EXPECT_FALSE(myInt::is_prime_num(727373));
    EXPECT_TRUE(myInt::is_prime_num(828383));
    EXPECT_TRUE(myInt::is_prime_num(929393));
}
