/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <gtest/gtest.h>
#include <algorithm>
#include <numeric>

// == project includes ==
#include "include/io.h"
#include "include/myInteger.h"
#include "include/largeInt.h"
#include "include/geometry.h"
#include "include/mytree.h"
#include "include/mydate.h"
#include "include/standalone.h"
#include "include/game.h"
#include "include/big_int.h"
#include "include/rational.h"

// given an integer number, rotates all digits to the right by one position.
// the last digit wraps around and becomes and leading digit.
int rotate_right(int number) {

    int num_digits = myInt(number).get_num_digits();

    if (num_digits ==1) {
        return number;
    }

    int last_digit = number % 10;
    int retval = last_digit;
    for (int i = 1; i < num_digits; ++i) {
        retval *= 10;
    }

    number /= 10;
    retval += number;

    return retval;

}

// given two arrays of digits, find and return the common digits in a new array
std::vector<int> find_common(std::vector<int> a, std::vector<int> b) {

    std::vector<int> common_d;
    common_d.clear();

    for(int i = 0; i < a.size(); ++i) {
        int k = a[i];
        std::vector<int>::iterator it = std::find(b.begin(), b.end(), k);

	if (it != b.end()) {
            common_d.push_back(k);
	    b.erase(it);
        }

    }

    return common_d;

}

bool have_same_digits(uint64_t a, uint64_t b) {
    
    std::vector<int> a_digits = myInt(a).get_digits();
    std::vector<int> b_digits = myInt(b).get_digits();

    std::vector<int> c = find_common(a_digits, b_digits);
    return (a_digits.size() == b_digits.size()) && (c == a_digits);

}
// =============================================================================
// ============================================================================= 
// k * (1, 2, 3, ... , 5)
std::vector<int> get_all_digits_from_multiple(int k, int n) {

    std::vector<int> combined_digits;

    for(int i = 1; i <= n; ++i) {
        uint64_t result = k * i;
        std::vector<int> digits = myInt(result).get_digits();
        combined_digits.insert(std::end(combined_digits), std::begin(digits), std::end(digits));
    }

    return combined_digits;
}

// =============================================================================
// input:
//      k * (1, 2, 3, ... , n)
//      k = [ibegin ~ iend]
// return:
//      array of pandigital numbers
std::vector<uint64_t> get_pandigital_vec(int ibegin, int iend, int nend) {

    std::vector<uint64_t> pandis;

    for(int k_iter = ibegin; k_iter <= iend; ++k_iter) {
        int current_k = k_iter;

        for (int n_iter = 1; n_iter <= nend; ++n_iter) {
            int current_n = n_iter;

            std::vector<int> combined_digits = get_all_digits_from_multiple(current_k, current_n);

            if (combined_digits.size() != 9) continue;

            bool is_pan = true;
            for (int i = 1; i <= 9; ++i) {
                if ( std::find(combined_digits.begin(), combined_digits.end(), i) == combined_digits.end() ) {
                    is_pan = false;
                    break;
                }
            }

            if (is_pan) {
                uint64_t value = largeInt<uint64_t>::get_value_from_digits(combined_digits);
                pandis.push_back(value);
            }

        }
        

    }

    return pandis;

}


int get_value_from_string(std::string in_str) {

    if (in_str.empty() == true)
        return 0;

    int sum = 0;

    for(std::string::iterator sit = in_str.begin(); sit != in_str.end(); ++sit) {
        int value = 1 + (*sit) - 'A';
        sum += value;
    }

    return sum;

}

bool sub_string_divisibility(std::vector<int> digits) {

    if (digits.size() != 10) return false;

    std::vector<int> primes({2, 3, 5, 7, 11, 13, 17});

    for (int i = 0; i < 7; ++i) {
        std::vector<int>::const_iterator first = digits.begin() + i + 1;
        std::vector<int>::const_iterator last  = digits.begin() + i + 4;
        std::vector<int> sub_digits(first, last);

        EXPECT_EQ(sub_digits.size(), 3);

        int value = largeInt<int>::get_value_from_digits(sub_digits);

        if (value % primes[i] != 0)
            return false;
    }

    return true;
}


bool is_distinct(std::vector<int> in_vec) {

    std::set<int> unique_vec;

    for (int i = 0; i < in_vec.size(); ++i) {
        unique_vec.insert(in_vec[i]);
    }

    return ( in_vec.size() == unique_vec.size() );
}


bool has_four_distinct_prime_factors(uint64_t a) {

    myInt test_int(a);

    std::vector<int> my_prime_divisors = test_int.get_prime_divisors();

    if (my_prime_divisors.size() != 4) return false;

    return is_distinct(my_prime_divisors);
}

uint64_t last_ten_digits_of_self_power_sum(int top_index) {
    
    uint64_t last_ten_digits_of_sum = 0;

    for (int i = 1; i <= top_index; i++) {
        int current_number = i;
        uint64_t last_ten_digits_of_power = 1;

        for (int count = 1; count <= current_number; count++) {
            last_ten_digits_of_power = (last_ten_digits_of_power * current_number) % 10000000000;
        }

        last_ten_digits_of_sum += last_ten_digits_of_power;
        last_ten_digits_of_sum = last_ten_digits_of_sum % 10000000000;

    }

    return last_ten_digits_of_sum;

}


bool ConcatIntoPrimes(int a, int b) {
    int c = largeInt<int>::concate_two_ints(a, b);
    int d = largeInt<int>::concate_two_ints(b, a);

    return (largeInt<int>::is_prime_num(c) && largeInt<int>::is_prime_num(d));
}

std::vector< std::set<int> > get_prime_sets_of(int set_size, std::map<std::pair<int, int>, bool> & data_base, std::vector<int> & primes) {

    std::vector< std::set<int> > retval;

    if (set_size == 1) {
        for (auto p : primes) {
            std::set<int> pset;
            pset.insert(p);
            retval.push_back(pset);
        }
    } else {
        std::vector< std::set<int> > prime_set_array_minus_one = get_prime_sets_of(set_size - 1, data_base, primes);
        
        for (int i : primes) {

            // lets loop over the existing prime sets
            for (int j = 0; j < prime_set_array_minus_one.size(); j++) {
                std::set<int> current_prime_set = prime_set_array_minus_one[j];
                std::set<int>::iterator it;

                bool found_non_prime_after_concat = false;

                // loop over prime numbers in the current set
                for (int current_prime_number : current_prime_set) {
                    
                    // concatenate with i, and check if the results are primes
                    if (false == data_base[std::make_pair(i, current_prime_number)]) {
                        found_non_prime_after_concat = true;
                        break;
                    }

                }

                // if all results are new primes, we have found one new set, add it to retval
                if (found_non_prime_after_concat == false) {
                    current_prime_set.insert(i);
                    retval.push_back(current_prime_set);
                }
            }
        }

    }

    return retval;
}

bool check_six_numbers(std::vector<int> in_numbers, std::map<int, int> & rMap) {
    std::set<int> unique_numbers;

    for (int i : in_numbers) {

        int polygon_type = rMap[i];
        unique_numbers.insert(polygon_type);
    }

    if (unique_numbers.size() == 6) return true;
    return false;

}

void build_cyclic_vector(int & rInputNum, 
                         std::map<int, std::vector<int>> & rCyclicMap,
                         std::map<int, int> & rNumberPolygonMap,
                         std::vector<int> InputCyclicArray) {
    std::vector<int> cyclic_array_local_copy;
    for (int i : InputCyclicArray) {
        cyclic_array_local_copy.push_back(i);
    }
    cyclic_array_local_copy.push_back(rInputNum);
    std::vector<int> downstream_numbers = rCyclicMap[rInputNum];
    for(int i : downstream_numbers) {

        if (i == cyclic_array_local_copy[0]) {
            if (cyclic_array_local_copy.size() == 6 && check_six_numbers(cyclic_array_local_copy, rNumberPolygonMap) == true) {
                
                std::cout << "Found SIX!!!!!!" << std::endl;
                for (int k : cyclic_array_local_copy) {
                    
                    std::cout << k << std::endl;

                }
                std::cout << "SUM = " << std::accumulate(cyclic_array_local_copy.begin(), cyclic_array_local_copy.end(), 0) << std::endl;

            }
            return;
        } else {

            if (cyclic_array_local_copy.size() == 6) return;
            build_cyclic_vector(i, rCyclicMap, rNumberPolygonMap, cyclic_array_local_copy);
        }

    }

}

void build_permutation_vector(uint64_t & rInputNum, std::map<uint64_t, std::vector<uint64_t>> & rCubeToPermutationMap, std::vector<uint64_t> list_of_permutations)
{
    std::vector<uint64_t> list_of_permutations_local_copy;
    for (auto i : list_of_permutations) {
        list_of_permutations_local_copy.push_back(i);
    }

    list_of_permutations_local_copy.push_back(rInputNum);

    std::vector<uint64_t> downstream_numbers = rCubeToPermutationMap[rInputNum];

    if (downstream_numbers.size() == 0) {
        int total_number_of_permutations = list_of_permutations_local_copy.size();
        if (total_number_of_permutations == 5) {
            std::cout << "Found FIVE!!!!" << std::endl;
            for(auto k : list_of_permutations_local_copy) {
                std::cout << k << std::endl;
            }
        } else {
//            std::cout << total_number_of_permutations << std::endl;
        }

    } else {

        for(auto i : downstream_numbers) {
            build_permutation_vector(i, rCubeToPermutationMap, list_of_permutations_local_copy);
        }

    }
}

int get_floor_of_sqrt(int n) {

    double n_sqrt = std::sqrt(n);
    return std::floor(n_sqrt);

}



void transform_for_problem64(int in_num_under_sqrt, int in_num_negative, int in_num_bottom, int & out_num_integer, int & out_num_under_sqrt, int & out_num_negative, int & out_num_bottom) {

    // step 1 -- flip
    int s1_num_under_sqrt = in_num_under_sqrt;
    int s1_num_negative = in_num_negative;
    int s1_num_top = in_num_bottom;

    // step 2 -- convert to plus sign
    int value = s1_num_under_sqrt - s1_num_negative * s1_num_negative;
    int s2_num_under_sqrt = s1_num_under_sqrt;
    int s2_num_positive = 0 - s1_num_negative;
    int s2_num_bottom = value / s1_num_top;

    // step 3 -- extract integer

    int s3_num_integer = 1;
    int s3_num_under_sqrt = s2_num_under_sqrt;
    int s3_num_negative = 0;  // temp value
    int s3_num_bottom = s2_num_bottom;    // temp value
    while (1) {

        s3_num_negative = s2_num_positive - s3_num_integer * s3_num_bottom;

        if (s3_num_under_sqrt - s3_num_negative * s3_num_negative < 0) {
            break;
        } else {
            s3_num_integer++;
        }

    }

    // go back one step
    s3_num_integer--;
    s3_num_negative = s2_num_positive - s3_num_integer * s3_num_bottom;

    // return values
    out_num_integer = s3_num_integer;
    out_num_under_sqrt = s3_num_under_sqrt;
    out_num_negative = s3_num_negative;
    out_num_bottom = s3_num_bottom;
}

int get_period_for_problem64(int in_num) {

    std::vector<int> period_vec;
        
    // first get floor
    int a0 = get_floor_of_sqrt(in_num);

    int out_num_integer = 0;
    int out_num_under_sqrt = 0;
    int out_num_negative = 0;
    int out_num_bottom = 0;
    // first iteration
    int in_num_under_sqrt = in_num;
    int in_num_negative = 0 - a0;
    int in_num_bottom = 1;
    transform_for_problem64(in_num_under_sqrt, in_num_negative, in_num_bottom, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);
    period_vec.push_back(out_num_integer);

    int ref_num_negative = out_num_negative;
    int ref_num_bottom = out_num_bottom;
    // start the loop
    while (1) {
        in_num_under_sqrt = out_num_under_sqrt;
        in_num_negative = out_num_negative;
        in_num_bottom = out_num_bottom;

        transform_for_problem64(in_num_under_sqrt, in_num_negative, in_num_bottom, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);
        
        if (out_num_negative == ref_num_negative && out_num_bottom == ref_num_bottom) {
            break;
        } else {
            period_vec.push_back(out_num_integer);
        }

    }

    return period_vec.size();

} 


int digit_of_continued_fraction_of_e(int i) {

    if (i == 1) {
        return 2;
    } else {
        if (i % 3 == 2) {
            return 1;
        }
        else if (i % 3 == 0) {
            return i / 3 * 2;
        } else { // if (i % 3 == 1)
            return 1;
        }
    }

}

Rational compute_continued_fraction(std::vector<int> in_digits) {

    Rational one("1/1");

    int num_digits = in_digits.size();

    if (num_digits == 1) {
        BigInt above(in_digits[0]);
        BigInt below(1);
        Rational value(above, below);
        return value;
    } else {

        // first digit
        BigInt above(in_digits[0]);
        BigInt below(1);
        Rational value(above, below);

        std::vector<int> rest_of_digits;
        for (int i = 1; i < num_digits; i++) {
            rest_of_digits.push_back(in_digits[i]);
        }
       
        return value + one / compute_continued_fraction(rest_of_digits);
    }


}


// =============================================================================
// ======== Here begins the problem set ========================================
// =============================================================================
//
TEST (project_euler, problem65) {

    EXPECT_EQ(digit_of_continued_fraction_of_e(1), 2);
    EXPECT_EQ(digit_of_continued_fraction_of_e(2), 1);
    EXPECT_EQ(digit_of_continued_fraction_of_e(3), 2);
    EXPECT_EQ(digit_of_continued_fraction_of_e(4), 1);
    EXPECT_EQ(digit_of_continued_fraction_of_e(5), 1);
    EXPECT_EQ(digit_of_continued_fraction_of_e(6), 4);
    EXPECT_EQ(digit_of_continued_fraction_of_e(7), 1);
    EXPECT_EQ(digit_of_continued_fraction_of_e(8), 1);
    EXPECT_EQ(digit_of_continued_fraction_of_e(9), 6);
    EXPECT_EQ(digit_of_continued_fraction_of_e(10), 1);

    // validate continued fraction calculator
    std::vector<int> a;
    a.push_back(1);
    Rational value = compute_continued_fraction(a);
    BigInt above = value.GetAbove();
    BigInt below = value.GetBelow();
    EXPECT_TRUE(above == 1);
    EXPECT_TRUE(below == 1);

    // [1, 2]
    a.push_back(2);
    value = compute_continued_fraction(a);
    above = value.GetAbove();
    below = value.GetBelow();
    EXPECT_TRUE(above == 3);
    EXPECT_TRUE(below == 2);

    // [1, 2, 2]
    a.push_back(2);
    value = compute_continued_fraction(a);
    above = value.GetAbove();
    below = value.GetBelow();
    EXPECT_TRUE(above == 7);
    EXPECT_TRUE(below == 5);

    // e
    std::vector<int> e_digits = {2, 1, 2, 1, 1, 4, 1, 1, 6, 1};
    value = compute_continued_fraction(e_digits);
    above = value.GetAbove();
    below = value.GetBelow();
    EXPECT_TRUE(above == 1457);
    EXPECT_TRUE(below ==  536);
    EXPECT_EQ(above.DigitSum(), 17);

    // real solution starts here
    std::vector<int> e_hundred_digits;
    for (int i = 1; i <= 100; i++) {
        e_hundred_digits.push_back(digit_of_continued_fraction_of_e(i));
    }

    value = compute_continued_fraction(e_hundred_digits);
    above = value.GetAbove();
    EXPECT_EQ(above.DigitSum(), 272);
}

TEST (project_euler, problem64) {

    EXPECT_EQ(get_floor_of_sqrt(23), 4);
    EXPECT_EQ(get_floor_of_sqrt(2), 1);
    EXPECT_EQ(get_floor_of_sqrt(3), 1);
    EXPECT_EQ(get_floor_of_sqrt(5), 2);
    EXPECT_EQ(get_floor_of_sqrt(6), 2);
    EXPECT_EQ(get_floor_of_sqrt(7), 2);
    EXPECT_EQ(get_floor_of_sqrt(8), 2);
    EXPECT_EQ(get_floor_of_sqrt(10), 3);
    EXPECT_EQ(get_floor_of_sqrt(11), 3);
    EXPECT_EQ(get_floor_of_sqrt(12), 3);
    EXPECT_EQ(get_floor_of_sqrt(13), 3);

    int out_num_integer = 0;
    int out_num_under_sqrt = 0;
    int out_num_negative = 0;
    int out_num_bottom = 0;

    // line for a0
    transform_for_problem64(23, -4, 1, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);

    EXPECT_EQ(out_num_integer, 1);
    EXPECT_EQ(out_num_under_sqrt, 23);
    EXPECT_EQ(out_num_negative, -3);
    EXPECT_EQ(out_num_bottom, 7);


    // line for a1
    transform_for_problem64(23, -3, 7, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);

    EXPECT_EQ(out_num_integer, 3);
    EXPECT_EQ(out_num_under_sqrt, 23);
    EXPECT_EQ(out_num_negative, -3);
    EXPECT_EQ(out_num_bottom, 2);

    // line for a2
    transform_for_problem64(23, -3, 2, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);

    EXPECT_EQ(out_num_integer, 1);
    EXPECT_EQ(out_num_under_sqrt, 23);
    EXPECT_EQ(out_num_negative, -4);
    EXPECT_EQ(out_num_bottom, 7);

    // line for a3
    transform_for_problem64(23, -4, 7, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);

    EXPECT_EQ(out_num_integer, 8);
    EXPECT_EQ(out_num_under_sqrt, 23);
    EXPECT_EQ(out_num_negative, -4);
    EXPECT_EQ(out_num_bottom, 1);

    // line for a4
    transform_for_problem64(23, -4, 1, out_num_integer, out_num_under_sqrt, out_num_negative, out_num_bottom);

    EXPECT_EQ(out_num_integer, 1);
    EXPECT_EQ(out_num_under_sqrt, 23);
    EXPECT_EQ(out_num_negative, -3);
    EXPECT_EQ(out_num_bottom, 7);

    // test function to get period
    int period_size = get_period_for_problem64(2);
    EXPECT_EQ(period_size, 1);
    period_size = get_period_for_problem64(3);
    EXPECT_EQ(period_size, 2);
    period_size = get_period_for_problem64(5);
    EXPECT_EQ(period_size, 1);
    period_size = get_period_for_problem64(6);
    EXPECT_EQ(period_size, 2);
    period_size = get_period_for_problem64(7);
    EXPECT_EQ(period_size, 4);
    period_size = get_period_for_problem64(8);
    EXPECT_EQ(period_size, 2);

    period_size = get_period_for_problem64(10);
    EXPECT_EQ(period_size, 1);

    period_size = get_period_for_problem64(11);
    EXPECT_EQ(period_size, 2);

    period_size = get_period_for_problem64(12);
    EXPECT_EQ(period_size, 2);

    period_size = get_period_for_problem64(13);
    EXPECT_EQ(period_size, 5);

    // real solution starts here
    int counter = 0;
    for (int i = 2; i <= 13; i++) {
        if (largeInt<int>::is_perfect_square(i) == true) continue;

        int period_size_2 = get_period_for_problem64(i);
        if (period_size_2 % 2 == 1) counter++;

    }

    EXPECT_EQ(counter, 4);

    counter = 0;
    for (int i = 2; i <= 10000; i++) {
        if (largeInt<int>::is_perfect_square(i) == true) continue;

        int period_size_3 = get_period_for_problem64(i);
        if (period_size_3 % 2 == 1) counter++;

    }

    EXPECT_EQ(counter, 1322);

}

TEST (project_euler, problem63) {

    int counter = 0;
    for (int base_num = 1; base_num <= 9; base_num++) {
        BigInt base_big_int = base_num;
        for (int power = 1; power <= 100; power++) {
            BigInt value_big_int = 1;

            //computing nth power
            for (int i = 0; i < power; i++) {
                value_big_int = value_big_int * base_big_int;
            }

            int num_digits = value_big_int.NumDigits();

            if (num_digits == power) counter++;
            if (num_digits < power) break;

        }

    }
    EXPECT_EQ(counter, 49);
}

TEST (project_euler, problem62) {
    // create vector of cubes
    std::vector<uint64_t> array_of_cubes;
    // this is a map from the cube number to its digits, sorted in ascending order
    std::map<uint64_t, std::vector<int>> cube_to_sorted_digits_map;

    for (uint64_t i = 100; i <= 9999; i++) {
        uint64_t value = i*i*i;
        array_of_cubes.push_back(value);
        uint64_t a = value;
        std::vector<int> a_digits = myInt(a).get_digits();
        std::sort(a_digits.begin(), a_digits.end());
        cube_to_sorted_digits_map[value] = a_digits;
    }

    std::sort(array_of_cubes.begin(), array_of_cubes.end());

    int num_digits_of_999_cube = cube_to_sorted_digits_map[999*999*999].size();
    EXPECT_EQ(num_digits_of_999_cube, 9);


    // create map
    std::map<uint64_t, std::vector<uint64_t>> cube_to_permutation_map;

    for (auto current_cube_number : array_of_cubes) {

        std::vector<uint64_t> list_of_permutations;

        std::vector<int> a_digits = cube_to_sorted_digits_map[current_cube_number];
        for (auto potential_permutation : array_of_cubes) {
            if ( potential_permutation > current_cube_number ) { // only pick permutations which are larger
                std::vector<int> b_digits = cube_to_sorted_digits_map[potential_permutation]; 
                if (a_digits == b_digits) {
//                    std::cout << "Found Permutation" << std::endl;
                    list_of_permutations.push_back(potential_permutation);
                }

            }
        }

        cube_to_permutation_map[current_cube_number] = list_of_permutations;

    }

    // run loop to find out all permutations
    for (auto i : array_of_cubes) {
        std::vector<uint64_t> list_of_cubes;
        build_permutation_vector(i, cube_to_permutation_map, list_of_cubes);
    }

}

TEST (project_euler, problem61) {

    std::vector<int> triangles;
    std::vector<int> squares;
    std::vector<int> pentagonals;
    std::vector<int> hexagonals;
    std::vector<int> heptagonals;
    std::vector<int> octagonals;

    // generate tria data arrays
    for (int n = 1; n > 0; n++) {
        int new_triangle = n * (n + 1) / 2;
        if (new_triangle <= 9999) {
            
            triangles.push_back(new_triangle);

        } else {

            break;

        }

    }
    // verify problem data
    EXPECT_EQ(triangles[126], 8128);

    // generate squares
    for (int n = 1; n > 0; n++) {
        int new_square = n * n;
        if (new_square <= 9999) {
            squares.push_back(new_square);
        } else {
            break;
        }
    }
    EXPECT_EQ(squares[90], 8281);

    // generate pentas
    for (int n = 1; n > 0; n++) {
        int new_penta = n * (3 * n - 1) / 2;
        if (new_penta <= 9999) {
            pentagonals.push_back(new_penta);
        } else {
            break;
        }
    }
    EXPECT_EQ(pentagonals[43], 2882);

    // hexas
    for (int n = 1; n > 0; n++) {
        int new_hexa = n * (2 * n - 1);
        if (new_hexa <= 9999) {
            hexagonals.push_back(new_hexa);
        } else {
            break;
        }
    }
    EXPECT_EQ(hexagonals[4], 45);

    // hepta
    for (int n = 1; n > 0; n++) {
        int new_hepta = n * (5 * n - 3) / 2;
        if (new_hepta <= 9999) {
            heptagonals.push_back(new_hepta);
        } else {
            break;
        }
    }
    EXPECT_EQ(heptagonals[4], 55);

    // octa
    for (int n = 1; n > 0; n++) {
        int new_octa = n * (3 * n - 2);
        if (new_octa <= 9999) {
            octagonals.push_back(new_octa);
        } else {
            break;
        }
    }
    EXPECT_EQ(octagonals[4], 65);


    auto is_cyclic = [](int i, int j) { return ((i % 100) == (j / 100));};

    std::vector<int> reduced_triangles;
    std::vector<int> reduced_squares;
    std::vector<int> reduced_pentagonals;
    std::vector<int> reduced_hexagonals;
    std::vector<int> reduced_heptagonals;
    std::vector<int> reduced_octagonals;


    std::vector<int> all_numbers;
    std::map<int, int> number_polygon_map;
    for(int i : triangles) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_triangles.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 3;
        }
    }
    for(int i : squares) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_squares.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 4;
        }
    }
    for(int i : pentagonals) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_pentagonals.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 5;
        }
    }
    for(int i : hexagonals) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_hexagonals.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 6;
        }
    }
    for(int i : heptagonals) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_heptagonals.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 7;
        }
    }
    for(int i : octagonals) {
        if ((i >= 1000) && (i % 100 >= 10)) {
            reduced_octagonals.push_back(i);
            all_numbers.push_back(i);
            number_polygon_map[i] = 8;
        }
    }


    std::map<int, std::vector<int>> downstream_cyclic_numbers;


    for (int i : all_numbers) {
        std::vector<int> downstream_vec;

        for (int j : all_numbers) {
            if (is_cyclic(i, j)) {
                downstream_vec.push_back(j);
            }

        }

        downstream_cyclic_numbers[i] = downstream_vec;


    }

    for (int i : all_numbers) {
//        std::cout << i << std::endl;
        std::vector<int> cyclic_array;
        build_cyclic_vector(i, downstream_cyclic_numbers, number_polygon_map, cyclic_array);
    }

}

TEST (project_euler, problem60) {

    // verify problem data
    int a = 3;
    int b = 7;
    int c = 109;
    int d = 673;

    EXPECT_TRUE(largeInt<int>::is_prime_num(a));
    EXPECT_TRUE(largeInt<int>::is_prime_num(b));
    EXPECT_TRUE(largeInt<int>::is_prime_num(c));
    EXPECT_TRUE(largeInt<int>::is_prime_num(d));

    EXPECT_TRUE (ConcatIntoPrimes(b, c));
    EXPECT_TRUE (ConcatIntoPrimes(d, c));



    // real solution starts
    //
    // 1. generate and store primes under 1,000,000
    std::vector<int> primes;
    for (int i = 2; i <= 10000; i++) {
        if (true == myInt::is_prime_num(i)) {
            primes.push_back(i);
        }
    }

    // 2. create a data_base of IntPair, bool to store the results of concatenating two primes
    typedef std::pair<int, int> IntPair;
    std::map<IntPair, bool> data_base;

    int counter = 0;
    for (auto prime_a : primes) {

        for (auto prime_b : primes) {
            if (prime_a == prime_b) { // same number, set to false
                data_base[std::make_pair(prime_a, prime_b)] = false;
            } else {
                data_base[std::make_pair(prime_a, prime_b)] = ConcatIntoPrimes(prime_a, prime_b);
                if (data_base[std::make_pair(prime_a, prime_b)] == true) {
                    counter++;
                }
            }
        }
    }

    EXPECT_TRUE (data_base[std::make_pair(3, 7)]);
    EXPECT_TRUE (data_base[std::make_pair(7, 109)]);
    EXPECT_TRUE (data_base[std::make_pair(109, 7)]);


    // again verify the problem data on 4-prime sets    
    std::vector< std::set<int> > four_prime_sets = get_prime_sets_of(4, data_base, primes);
    int min_sum = 10000000;
    for (auto current_set : four_prime_sets) {
        int sum = 0;
        for (int i : current_set) {
            sum += i;
        }

        min_sum = std::min(min_sum, sum);
    }
    EXPECT_EQ(min_sum, 792);


    // solution for 5-prime sets
    std::vector< std::set<int> > five_prime_sets = get_prime_sets_of(5, data_base, primes);
    min_sum = 10000000;
    for (auto current_set : five_prime_sets) {
        int sum = 0;
        for (int i : current_set) {
            sum += i;
        }

        min_sum = std::min(min_sum, sum);
    }
    EXPECT_EQ(min_sum, 26033);
    //
    // DONE
}

TEST (project_euler, problem59) {

    // verify xor operation
    char a = 'A';
    char b = '*';
    char c = a ^ b;
    EXPECT_EQ(c, 'k');

    char d = c ^ b;
    EXPECT_EQ(d, 'A');

    // begin solution
    std::string token(",");
    std::vector<std::string> codes = ImportRawDataFrom("../Data/p059_cipher.txt").GetStringsDividedBy(token);

    int value_sum = 0;

    for (int i = 0; i < 26; i++) {
        a = 'a' + i;

        for (int j = 0; j < 26; j++) {
            b = 'a' + j;

            for (int k = 0; k < 26; k++) {
                c = 'a' + k;
                
                // now i have 'abc' as my key
                // let's decipher the text
                std::string deciphered_text;
                int counter = 0;
                for (auto code : codes) {
                    d = std::stoi(code);

                    if (counter % 3 == 0) { // xor with a
                        char e = d ^ a;
                        deciphered_text.push_back(e);
                    } else if ( counter % 3 == 1) { // xor with b
                        char e = d ^ b;
                        deciphered_text.push_back(e);
                    } else { // xor with c
                        char e = d ^ c;
                        deciphered_text.push_back(e);
                    }

                    counter = counter + 1;
                }

                // now we have the deciphered text,
                // we need to look for a couple common english words.
                // e.g. "the", "this"
                //
                std::string common_word_the  = "the";
                std::string common_word_this = "this";

                if (deciphered_text.find(common_word_the) != std::string::npos) {
                    if (deciphered_text.find(common_word_this) != std::string::npos) {
                        // found the original text,
                        // lets compute the sum
                        value_sum = 0;
                        for (char m : deciphered_text) {
                            int code_value = m;
                            value_sum = value_sum + code_value;
                        }
                    }
                }

            }
        }
    }

    EXPECT_EQ(value_sum, 129448);

}

TEST (project_euler, problem58) {
    auto get_corner_numbers = [](uint64_t i) { std::vector<uint64_t> a{i*i, i*i - (i-1), i*i - 2*(i-1), i*i - 3*(i-1) }; return a;};
    auto get_diag_number_count = [](uint64_t i) { return (2*(i+1) - 3); };

    std::vector<uint64_t> n7 = get_corner_numbers(7);

    EXPECT_EQ(n7[0], 49);
    EXPECT_EQ(n7[1], 43);
    EXPECT_EQ(n7[2], 37);
    EXPECT_EQ(n7[3], 31);

    uint64_t diag_number_counter = get_diag_number_count(7);
    EXPECT_EQ(diag_number_counter, 13);

    // begin solution
    //
    uint64_t final_side_length;
    std::vector<uint64_t> diag_prime_numbers;
    for (uint64_t j = 3; j > 0; j = j + 2) {

        uint64_t current_side_length = j;

        diag_number_counter = get_diag_number_count(current_side_length);
        std::vector<uint64_t> current_corner_numbers = get_corner_numbers(current_side_length);

        for (uint64_t k : current_corner_numbers) {
            if (true == myInt::is_prime_num(k)) {
                diag_prime_numbers.push_back(k);
            }
            
        }

        uint64_t prime_number_counter = diag_prime_numbers.size();

        if (prime_number_counter * 10 < diag_number_counter) {
            final_side_length = current_side_length;
            break;
        }


    }

    EXPECT_EQ(final_side_length, 26241);
    // end solution
}


TEST (project_euler, problem57) {

    // data given from problem 57 of project euler
    Rational half("1/2");
    Rational one("1");
    Rational two("2");
    // first expansion
    Rational c = one + half;
    BigInt ca = c.GetAbove();
    BigInt cb = c.GetBelow();
    EXPECT_TRUE(ca == 3);
    EXPECT_TRUE(cb == 2);
    // second
    Rational a = two + half;
    Rational b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 7);
    EXPECT_TRUE(c.GetBelow() == 5);
    
    // third
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 17);
    EXPECT_TRUE(c.GetBelow() == 12);

    // fourth
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 41);
    EXPECT_TRUE(c.GetBelow() == 29);

    // 5th
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 99);
    EXPECT_TRUE(c.GetBelow() == 70);

    // 6th
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 239);
    EXPECT_TRUE(c.GetBelow() == 169);

    // 7th
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 577);
    EXPECT_TRUE(c.GetBelow() == 408);

    // 8th
    a = two + b;
    b = one / a;
    c = one + b;
    EXPECT_TRUE(c.GetAbove() == 1393);
    EXPECT_TRUE(c.GetBelow() == 985);

    int counter = 1;

    for (int i = 9; i <= 1000; i++) {
        a = two + b;
        b = one / a;
        c = one + b;
        ca = c.GetAbove();
        cb = c.GetBelow();
        if (ca.GetValueAsStr().size() > cb.GetValueAsStr().size()) {
            counter = counter + 1;
        }
       
    }

    EXPECT_EQ(counter, 153);
}


TEST (project_euler, problem56) {

    unsigned int max_digit_sum = 0;

    for (BigInt i = 1; i < 100; i = i + 1) {
        for (unsigned int exp = 1; exp < 100; exp = exp + 1) {
            BigInt result = i^exp;
            unsigned int current_digit_sum = result.DigitSum();
            max_digit_sum = std::max(max_digit_sum, current_digit_sum);
        }
    }

    EXPECT_EQ(max_digit_sum, 972);
}

TEST (project_euler, problem55) {

    // begin verification

    BigInt a = 47;
    unsigned int num_iter;
    bool is_lychrel = a.IsLychrel(num_iter);
    EXPECT_FALSE(is_lychrel);
    EXPECT_EQ(num_iter, 1);
    
    BigInt b = 349;
    is_lychrel = b.IsLychrel(num_iter);
    EXPECT_FALSE(is_lychrel);
    EXPECT_EQ(num_iter, 3);

    BigInt c = 196;
    is_lychrel = c.IsLychrel(num_iter);
    EXPECT_TRUE(is_lychrel);

    BigInt d = 10677;
    is_lychrel = d.IsLychrel(num_iter);
    EXPECT_TRUE(is_lychrel);

    unsigned int max_iter = 60;
    is_lychrel = d.IsLychrel(max_iter, num_iter);
    EXPECT_FALSE(is_lychrel);
    EXPECT_EQ(num_iter, 53);

    // end of verification

    // begin solution
    unsigned int counter = 0;
    for (signed long int i = 5; i <= 9999; i++) {
        BigInt e(i);
        if (e.IsLychrel(num_iter)) {
            counter = counter + 1;
        }
    }
    EXPECT_EQ(counter, 249);
    // end of solution
}

TEST (project_euler, problem54) {
    int counter = 0;

    std::ifstream data_file;
    data_file.open("../Data/p054_poker.txt", std::ifstream::in);

    std::string deck;
    while (getline (data_file, deck))
    {
        Poker::Game g(deck);
        if (g.Player1Wins()) {
            counter = counter + 1;
        }
    }

    data_file.close();

    // verify result
    EXPECT_EQ(counter, 376);
    
}

TEST (project_euler, problem53) {

    BigInt a = 5;
    BigInt test_result = largeInt<BigInt>::factorial(a);
    EXPECT_TRUE (test_result == 120);

    BigInt comb_result = largeInt<BigInt>::combinatory(2, 5);
    EXPECT_TRUE (comb_result == 10);
    ////
    int counter = 0;

    for (int i = 1; i <= 100; i++) {
        for (int j = 1; j <= i; j++ ) {
            BigInt result = largeInt<BigInt>::combinatory(j, i);
            if (result > 1000000) {
                counter = counter + 1;
            }
        }
    }
    EXPECT_EQ(counter, 4075);
}

TEST (project_euler, problem52) {

    uint64_t a  = 125874;
    uint64_t aa = a * 2;

    EXPECT_TRUE( have_same_digits(a, aa) );

    uint64_t result = 0;

    for (uint64_t i = 1; i < 1000000; i++) {
        uint64_t b = i;
        uint64_t bb = b * 2;
        uint64_t bbb = b * 3;
        uint64_t bbbb = b * 4;
        uint64_t bbbbb = b * 5;
        uint64_t bbbbbb = b * 6;

        bool found = false;

        if ( have_same_digits(b, bb) == true ) {
            if ( have_same_digits(b, bbb) == true ) {
                if ( have_same_digits(b, bbbb) == true ) {
                    if ( have_same_digits(b, bbbbb) == true ) {
                        if ( have_same_digits(b, bbbbbb) == true ) {
                            found = true;
                        }
                    }
                }
            }
        }

        if (found) {
            result = b;
            break;
        }
    }

    EXPECT_EQ(result, 142857);

}

TEST (project_euler, problem51) {
    // 1. generate array of primes
    //
    // 2. scan the primes one by one
    //
    // 3. pick the ones with at least 2 repeating digits
    //
    // 4. generate a ten-number family by replacing the repeating digits with 0..9
    //
    // 5. check if the family contains 8 primes.
    //
    // 6. first, test on the 7-prime result given by the problem data.

    // 1.
    std::vector<uint64_t> primes;
    for (uint64_t i = 2; i <= 1000000; i++) {
        if (true == myInt::is_prime_num(i)) {
            primes.push_back(i);
        }
    }

    // NOTE: for "13", this algorithm does not apply. 
    // we have to test without assuming repeating digits, since there are only 2 digits in the number.
    uint64_t min_prime_7 = 2000000;
    uint64_t min_prime_8 = 2000000;

    // 2.
    for(auto i : primes) {
        // 3. find repeating digits
        uint64_t current_prime = i;

        std::vector<int> original_digits = myInt(current_prime).get_digits();
        std::map<int, std::vector<int>> repeating_digit_map = myInt(current_prime).get_repeating_digits_map();

        for(int j = 0; j <= 9; j++) {
            if (repeating_digit_map[j].size() >= 2) {
                // 4. generate a ten-number family
                std::vector<int> indices = repeating_digit_map[j];
                std::vector<uint64_t> ten_number_family;
                for (int k = 0; k <= 9; k++) {
                    std::vector<int> new_digits = original_digits;
                    for (int l = 0; l < indices.size(); l++) {
                        new_digits[indices[l]] = k;
                    }

                    int64_t value = largeInt<uint64_t>::get_value_from_digits(new_digits);
                    ten_number_family.push_back(value);
                }

                // verify if any number has "0" as the leading digit
                int size0 = myInt(ten_number_family[0]).get_digits().size();
                if (size0 != original_digits.size()) {
                    ten_number_family.erase(ten_number_family.begin());
                }

                //
                std::vector<uint64_t> primes_of_the_family;
                primes_of_the_family.clear();
                for (auto i_new_number : ten_number_family) {
                    if (true == myInt::is_prime_num(i_new_number)) {
                        primes_of_the_family.push_back(i_new_number);
                    }
                }

                if (primes_of_the_family.size() == 7) {
                    uint64_t current_min_prime = primes_of_the_family[0];
                    if (current_min_prime < min_prime_7) {
                        min_prime_7 = current_min_prime;
                    }
                }

                if (primes_of_the_family.size() == 8) {
                    uint64_t current_min_prime = primes_of_the_family[0];
                    if (current_min_prime < min_prime_8) {
                        min_prime_8 = current_min_prime;
                    }
                }
            }
        }
    }

    EXPECT_EQ(min_prime_7, 56003);
    EXPECT_EQ(min_prime_8, 121313);
}

TEST (project_euler, problem50) {
    std::vector<uint64_t> primes;
    for (uint64_t i = 2; i <= 1000000; i++) {
        if (true == myInt::is_prime_num(i)) {
            primes.push_back(i);
        }
    }

    uint64_t sum = 0;
    int index = 0;
    while (1) {
        sum += primes[index];
        index++;

        if (sum > 1000000) break;
    }
    
    int num_primes = index;

    bool found = false;
    while (1) {
        // decrease number of primes
        int num_of_consecutive_primes = index--;
        for (int i = 0; i < num_primes; i++) {
            sum = 0;
            for (int j = i; j < i + num_of_consecutive_primes; j++) {
                sum += primes[j];
            }
            if (sum < 1000000 && true == myInt::is_prime_num(sum)) {
                found = true;
                break;
            }
        }

        // get out of while loop
        if (found) break;
    }

    EXPECT_EQ(sum, 997651);
}

TEST (project_euler, problem49) {

    std::vector<uint64_t> expected_results;
    std::vector<uint64_t> computed_results;

    std::vector<uint64_t> four_digit_prime_numbers;

    for (uint64_t i = 1000; i <= 9999; i++) {
        if (true == myInt::is_prime_num(i)) {
            four_digit_prime_numbers.push_back(i);
        }
    }

    int num_of_primes = four_digit_prime_numbers.size();

    for (int i = 0; i < num_of_primes - 1; i++) {

        // get first prime and its digits, sort the digits
        uint64_t a = four_digit_prime_numbers[i];
        std::vector<int> a_digits = myInt(a).get_digits();
        std::sort(a_digits.begin(), a_digits.end());

        for (int j = i + 1; j < num_of_primes; j++) {
            // get second prime number and sort its digits
            uint64_t b = four_digit_prime_numbers[j];
            std::vector<int> b_digits = myInt(b).get_digits();
            std::sort(b_digits.begin(), b_digits.end());

            // if digits diff, not permutation, skip
            if (a_digits != b_digits) continue;

            // compute desired third number
            uint64_t c = 2 * b - a;

            // check if it's in the prime list. if not, then skip
            std::vector<uint64_t>::iterator it;
            it = find (four_digit_prime_numbers.begin(), four_digit_prime_numbers.end(), c);
            if (it == four_digit_prime_numbers.end()) continue;

            // get its digits and sort
            std::vector<int> c_digits = myInt(c).get_digits();
            std::sort(c_digits.begin(), c_digits.end());

            // if digits diff, not permutation, skip
            if (a_digits != c_digits) continue;

            uint64_t value1 = largeInt<uint64_t>::concate_two_ints(a, b);
            uint64_t value2 = largeInt<uint64_t>::concate_two_ints(value1, c);
            computed_results.push_back(value2);
        }

    }

    EXPECT_EQ(computed_results.size(), 2);
    EXPECT_EQ(computed_results[0], 148748178147);
    EXPECT_EQ(computed_results[1], 296962999629);
}

TEST (project_euler, problem48) {
    

    // verify algorithm with data given in problem
    uint64_t computed_result = last_ten_digits_of_self_power_sum(10);
    uint64_t given_result = 405071317;
    EXPECT_EQ(computed_result, given_result);

    // compute the actual problem and 
    computed_result = last_ten_digits_of_self_power_sum(1000);
    given_result    = 9110846700;
    EXPECT_EQ(computed_result, given_result);

}

TEST (project_euler, problem47) {

    myInt val(644);
    std::vector<int> my_prime_divisors = val.get_prime_divisors();
    EXPECT_EQ(my_prime_divisors.size(), 3);
    
    EXPECT_EQ(my_prime_divisors[0], 2);
    EXPECT_EQ(my_prime_divisors[1], 7);
    EXPECT_EQ(my_prime_divisors[2], 23);


    std::map<uint64_t, bool> qualified;

    for (uint64_t j = 1; j <= 4; j++) {
        qualified[j] = false;
    }
    
    uint64_t i = 5;
    while(1) {
        qualified[i] = has_four_distinct_prime_factors(i);

        if ( qualified[i] && qualified[i-1] && qualified[i-2] && qualified[i-3] ) {
            break;
        } else {
            i++;
        }
    }    

    uint64_t first_num = i - 3;
    EXPECT_EQ(134043, first_num);

}

TEST (project_euler, problem46) {

    std::vector<uint64_t> primes;
    primes.push_back(2);
    primes.push_back(3);
    primes.push_back(5);
    primes.push_back(7);

    uint64_t target = 7;

    while(1) {

        target = target + 2;

        // check primity
        if ( myInt::is_prime_num(target) == true ) {

            primes.push_back(target);
            continue;

        } else { // we have a composite odd number here

            int num_primes = primes.size();
            bool found = false;

            for (int i = 1; i < num_primes; ++i) {
                uint64_t half_residual = (target - primes[i]) / 2;

                if (largeInt<uint64_t>::is_perfect_square(half_residual) == true) {
                    found = true;
                    break;
                }
            }

            if (found == false) { // found the desired number, stop the while loop
                break;
            }

        }

    } // end while

    EXPECT_EQ(5777, target);

}

TEST (project_euler, problem45) {

    uint64_t target = 0;
    int index = 0;
    int counter = 0;
    for (uint64_t n = 286; n > 0; ++n) {
        uint64_t num = n * (n + 1) / 2;

        if (largeInt<uint64_t>::is_pentagon_number(num) == false) continue;

        if (largeInt<uint64_t>::is_hexagon_number(num) == false) continue;

        target = num;
        index = n;
        
        break;
    }

    EXPECT_EQ(target, 1533776805);
    EXPECT_EQ(index, 55385);
}

TEST (project_euler, problem44) {

    // tests against info given in problem
    EXPECT_FALSE(largeInt<uint64_t>::is_pentagonal_pair(22, 70));
    EXPECT_EQ(35, largeInt<uint64_t>::gen_penta_number(5));

    // here comes the real stuff -- brute force search !!!
    std::vector<uint64_t> penta_numbers;
    uint64_t min_diff = 1000000000;

    int num_pentas = 5000;
    for(int i = 1; i <= num_pentas; ++i) {
        penta_numbers.push_back(largeInt<uint64_t>::gen_penta_number(i));
    }

    for (int i = 0; i < num_pentas - 1; ++i) {
        for (int j = i + 1; j < num_pentas; ++j) {
            if (largeInt<uint64_t>::is_pentagonal_pair(penta_numbers[i], penta_numbers[j])) {
                uint64_t new_diff = penta_numbers[j] - penta_numbers[i];
                min_diff = new_diff < min_diff ? new_diff : min_diff;
            }
        }
    }

    EXPECT_EQ(5482660, min_diff);

}

TEST (project_euler, problem43) {

    std::vector<int> digits = myInt(1406357289).get_digits();
    EXPECT_EQ(10, digits.size());

    EXPECT_TRUE(sub_string_divisibility(digits));

    uint64_t sum = 0;

    do {
        if (digits[0] == 0) continue;

        if (sub_string_divisibility(digits) == true) {
            sum += largeInt<uint64_t>::get_value_from_digits(digits);
        }

    } while(std::next_permutation(digits.begin(), digits.end()));

    uint64_t expected = 16695334890;

    EXPECT_EQ(sum, expected);
}

TEST (project_euler, problem42) {

    using namespace std;

    string token("\",");
    vector<string> words = ImportRawDataFrom("../Data/p042_words.txt").GetStringsDividedBy(token);

    int total_num = 0;

    for (int i = 0; i < words.size(); ++i) {
        if ( largeInt<uint64_t>::is_triangle_number( get_value_from_string(words[i]) ) == true ) {
            total_num ++;
        }
    }
        
    EXPECT_EQ(55, get_value_from_string("SKY"));
    EXPECT_TRUE( largeInt<uint64_t>::is_triangle_number(55) );
    EXPECT_FALSE( largeInt<uint64_t>::is_triangle_number(0) );
    EXPECT_EQ(162, total_num);
}

TEST (project_euler, problem41) {

    // tests against info given by the problem
    //
    EXPECT_EQ(true, myInt(2143).is_prime());
    EXPECT_EQ(true, myInt(2143).is_pandigital());

    uint64_t peak_prime = 0;

    std::vector<int> digits = myInt(1234567).get_digits();

    // generate pandigital numbers, instead of brute-force searching
    do {

        uint64_t value = largeInt<uint64_t>::get_value_from_digits(digits);

        if (myInt(value).is_prime() == true) {
            if (value > peak_prime) {
                peak_prime = value;
            }
        }

    } while(std::next_permutation(digits.begin(), digits.end()));

    uint64_t expected = 7652413;

    EXPECT_EQ(peak_prime, expected);

}

TEST (project_euler, problem40) {
    
    std::vector<int> fraction_digits;

    for (int i = 0; i >= 0; ++i) {
        
        int num_digits = fraction_digits.size();

        if (num_digits > 1000000) {
            continue;
        }

        std::vector<int> new_digits = myInt(i).get_digits();

        fraction_digits.insert(fraction_digits.end(), new_digits.begin(), new_digits.end());

    }

    int retval = fraction_digits[12];

    EXPECT_EQ(1, retval);

    int value = fraction_digits[1] *fraction_digits[10] *fraction_digits[100] *fraction_digits[1000] *fraction_digits[10000] *fraction_digits[100000] *fraction_digits[1000000];

    EXPECT_EQ(210, value);

}

TEST (project_euler, problem39) {

    int side1 = 1;
    int side2 = 1;
    int side3 = 1;

    std::map<int, int> num_right_angle_tri_for_peri;

    for (int i = 0; i <= 1000; ++i)
        num_right_angle_tri_for_peri[i] = 0;

    for (side1 = 1; side1 <= 1000; ++side1) {
        for (side2 = side1; side2 <= 1000; ++side2) {
            for (side3 = side2; side3 <= 1000; ++side3) {

                int perimeter = side1 + side2 + side3;
                
                if (perimeter > 1000) continue;

                if (geometry<int>::is_right_angle_triangle(side1, side2, side3) == true) {
                    num_right_angle_tri_for_peri[perimeter]++;
                }
            }
        }
    }

    EXPECT_EQ(3, num_right_angle_tri_for_peri[120]);

    int max_num_solns = 0;
    int max_num_peri  = 0;

    for (int i = 0; i <= 1000; ++i) {
        int my_soln = num_right_angle_tri_for_peri[i];
        if (my_soln > max_num_solns) {
            max_num_peri = i;
            max_num_solns = my_soln;
        }
    }

    EXPECT_EQ(8, max_num_solns);
    EXPECT_EQ(840, max_num_peri);

}

TEST (project_euler, problem38) {

    // validation test against an example given in the problem definition
    std::vector<uint64_t> pandi_1 = get_pandigital_vec(1    , 9    , 5);
    uint64_t given = 918273645;
    EXPECT_EQ(pandi_1[0], given);

    // here comes the serious stuff
    std::vector<uint64_t> pandi_2 = get_pandigital_vec(90   , 99   , 5);
    std::vector<uint64_t> pandi_3 = get_pandigital_vec(900  , 999  , 3);
    std::vector<uint64_t> pandi_4 = get_pandigital_vec(9000 , 9999 , 2);
    std::vector<uint64_t> pandi_5 = get_pandigital_vec(90000, 99999, 2);

    std::vector<uint64_t> local_max_pandi;
    if (pandi_2.size() > 0)
        local_max_pandi.push_back(*std::max_element(pandi_2.begin(), pandi_2.end()));
    if (pandi_3.size() > 0)
        local_max_pandi.push_back(*std::max_element(pandi_3.begin(), pandi_3.end()));
    if (pandi_4.size() > 0)
        local_max_pandi.push_back(*std::max_element(pandi_4.begin(), pandi_4.end()));
    if (pandi_5.size() > 0)
        local_max_pandi.push_back(*std::max_element(pandi_5.begin(), pandi_5.end()));

    uint64_t global_max = *std::max_element(local_max_pandi.begin(), local_max_pandi.end());
    uint64_t expected = 932718654;

    EXPECT_EQ(expected, global_max);
}

TEST (project_euler, problem37) {

    std::vector<uint64_t> truncate_primes;

    uint64_t current_number = 10;

    std::map<uint64_t, bool> is_prime;

    // store the single-digit ones first
    for(uint64_t i = 0; i <= 9; ++i) {
        is_prime[i] = largeInt<uint64_t>::is_prime_num(i);
    }


    for( ; current_number > 0; ++current_number) {
        is_prime[current_number] = largeInt<uint64_t>::is_prime_num(current_number);

        if (is_prime[current_number] == true) {

            // try to truncate from right
            uint64_t truncated = current_number;
            bool truncated_primes = true;
            while(truncated != 0) {
                truncated_primes = is_prime[truncated];
                if (truncated_primes == false) {
                    break;
                }
                truncated /= 10;
            }

            if (truncated_primes == false) {
                continue;
            }
            // try to truncate from left
            //
            std::vector<int> digits = myInt(current_number).get_digits();
//            std::reverse(digits.begin(), digits.end());
            for(int i = 1; i < digits.size(); ++i) {
                std::vector<int> part_digits;
                part_digits.clear();
                for(int j = i; j < digits.size(); ++j) {
                    part_digits.push_back(digits[j]);
                }
                truncated = largeInt<uint64_t>::get_value_from_digits(part_digits);
                truncated_primes = is_prime[truncated];
                if (truncated_primes == false) {
                    break;
                }
                
            }

            if (truncated_primes == false) {
                continue;
            }
                
            if (truncated_primes == true) {
                truncate_primes.push_back(current_number);

                if (truncate_primes.size() == 11) {
                    break;
                }
            }
        }

    }

    uint64_t sum = 0;
    for(int i=0; i < truncate_primes.size(); ++i)
        sum += truncate_primes[i];

    EXPECT_EQ(748317, sum);
}    
    
TEST (project_euler, problem36) {

    uint64_t sum = 0;

    for(int i = 0; i < 1000000; ++i) {

        // check palindrome for base 10
        if (BigInt(i).IsPalindromic() == false) continue;

        // check palindrome for base 2
        std::vector<int> bin_digits = largeInt<int>::decimal_to_binary_digits(i);
        int num_digits = bin_digits.size();
        std::vector<int> reversed_bin_digits;
        reversed_bin_digits.resize(num_digits);

        std::reverse_copy(bin_digits.begin(), bin_digits.end(), reversed_bin_digits.begin());

        if (bin_digits == reversed_bin_digits) {
            sum += i;
        }

    }

    EXPECT_EQ(872187, sum);


}


TEST (project_euler, problem35) {


    std::vector<int> prime_numbers;

    for(int i = 2; i <= 1000000; ++i) {
        if (largeInt<int>::is_prime_num(i) == true) {
            prime_numbers.push_back(i);
        }
    }

    std::vector<int> circular_primes;

    for (int i = 0; i < prime_numbers.size(); ++i) {
        int current_prime_number = prime_numbers[i];
        std::vector<int> all_digits = myInt(current_prime_number).get_digits();
        int num_digits = all_digits.size();

        if (num_digits == 1) {
            circular_primes.push_back(current_prime_number);
            continue;
        }

        int rotated_number = current_prime_number;
        bool found_non_prime = false;
        for (int j = 1; j < num_digits; ++j) {
            rotated_number = rotate_right(rotated_number);
            if (largeInt<int>::is_prime_num(rotated_number) == false) {
                found_non_prime = true;
                break;
            }
        }

        if (found_non_prime == false) {
            circular_primes.push_back(current_prime_number);
        }
    }

    EXPECT_EQ(55, circular_primes.size());
}

TEST (project_euler, problem34) {


    std::map<int, int> factorial_of;

    std::vector<int> find_numbers;

    // pre-compute factorial
    for(int i = 0; i <=9; ++i) {

        factorial_of[i] = largeInt<int>::factorial(i);
    }

    // finding all
    for(int j = 3; j <= 99999; ++j) {


        std::vector<int> digits = myInt(j).get_digits();
        int sum = 0;
        for( int k = 0; k < digits.size(); ++k ) {

            int the_kth_digit = digits[k];
            sum += factorial_of[the_kth_digit];

        }

        if (sum == j) {
            find_numbers.push_back(j);
        }
    }

    int retval = std::accumulate(find_numbers.begin(), find_numbers.end(), 0);

    EXPECT_EQ(retval, 40730);

}


TEST (project_euler, problem33) {

    int found_size = 0;
    int mul_denom = 1;
    int mul_numer = 1;

    for (int i = 10; i <= 98; ++i) {

        for (int j = i+1; j <= 99 ; ++j) {

            int numer = i;
            int denom = j;

            std::vector<int> numer_d = myInt(numer).get_digits();
            std::vector<int> denom_d = myInt(denom).get_digits();

            std::vector<int> common_d = find_common(numer_d, denom_d);

	    if (common_d.size() != 1) continue;


	    if (common_d[0] == 0) continue;


	    int numer_0 = 0;
	    int denom_0 = 0;
	    if (numer_d[0] == common_d[0]) {
                numer_0 = numer_d[1];
	    } else {
                numer_0 = numer_d[0];
	    }

            if (numer_0 == 0) continue;

	    if (denom_d[0] == common_d[0]) {
                denom_0 = denom_d[1];
	    } else {
                denom_0 = denom_d[0];
	    }

            if (denom_0 == 0) continue;

	    if ( numer * denom_0 == denom * numer_0 ) {
                found_size ++;
		mul_denom *= denom_0;
		mul_numer *= numer_0;
	    }
        }

    }

    int temp = largeInt<int>::get_gcd(mul_numer, mul_denom);
    EXPECT_EQ(mul_denom/temp, 100);
}



TEST (project_euler, problem32)
{
// unit testing of pandigital function
  myInt a(123456789);
  EXPECT_TRUE(a.is_pandigital());

  myInt b(7654321);
  EXPECT_TRUE(b.is_pandigital());

  myInt c(112345789);
  EXPECT_FALSE(c.is_pandigital());

// actual code for prob 32 begins from here

  uint64_t i = 0; 
  uint64_t j = 0;
  uint64_t k = i * j;

  std::set<uint64_t> pandigital_products;

  for (i = 1; i <= 54321; ++i) {
      
      for (j = 1; j <= 54321 / i; ++j) {

          k = i * j;

          if (k > 987654321) continue;

          myInt first(i);
          myInt second(j);
          myInt product(k);

          std::vector<int> first_d = first.get_digits();
          std::vector<int> second_d = second.get_digits();
          std::vector<int> product_d = product.get_digits();

          product_d.insert(product_d.end(), first_d.begin(), first_d.end());
          product_d.insert(product_d.end(), second_d.begin(), second_d.end());

          if (product_d.size() != 9) continue;

         if (std::find(product_d.begin(), product_d.end(), 0) != product_d.end()) continue;

          uint64_t final_number = 0;
         for ( int t = 0; t < product_d.size(); ++t ) {
              final_number = product_d[t] + final_number * 10;
          }

          myInt f(final_number);
          if (f.is_pandigital()) {
            pandigital_products.insert(k);
         }

      } // end for j

  } // end for i

    uint64_t retval = std::accumulate(pandigital_products.begin(),
            pandigital_products.end(),
            0);

    EXPECT_EQ(retval, 45228);
}

TEST (project_euler, problem31)
{
  std::stack<int> list_of_denom;
  list_of_denom.push(1);
  list_of_denom.push(2);
  list_of_denom.push(5);
  list_of_denom.push(10);
  list_of_denom.push(20);
  list_of_denom.push(50);
  list_of_denom.push(100);
  list_of_denom.push(200);

  EXPECT_EQ(make_money(200, list_of_denom), 73682);
}

TEST (project_euler, problem30)
{

    myInt t1(1634);
    EXPECT_EQ(t1.power_sum(4), t1.value());
    myInt t2(8208);
    EXPECT_EQ(t2.power_sum(4), t2.value());
    myInt t3(9474);
    EXPECT_EQ(t3.power_sum(4), t3.value());

    uint64_t sum = 0;
    uint64_t i;
    int num_int = 0;
    for(i = 2; i < 1000000; ++i) {
        myInt a(i);
        if ( a.power_sum(5) == a.value() ) {
            sum += a.value();
	    num_int += 1;
	}
    }

    EXPECT_EQ(num_int, 6);
    EXPECT_EQ(sum, 443839);
}

TEST (project_euler, problem29)
{
    std::string cvalue;
    std::set<std::string> cvalue_set;
    BigInt mybase = 2;
    unsigned long int myexp = 2;
    for (mybase = 2; mybase <= 100; mybase = mybase + 1) {
        for (myexp = 2; myexp <= 100; ++myexp) {
            BigInt value = mybase^myexp;
	        cvalue = value.GetValueAsStr();
	        cvalue_set.insert(cvalue);
	    }
    }

    EXPECT_EQ(cvalue_set.size(), 9183);
}


TEST (project_euler, problem28)
{
    EXPECT_EQ(largeInt<int>::compute_diag(5), 101);
    EXPECT_EQ(largeInt<uint64_t>::compute_diag(1001),669171001);
}

TEST (project_euler, problem27)
{
    int a, b, n, max_n = 0;
    uint64_t uvalue;
    long long value, prod_coeff;
    for ( a = -999; a < 1000; ++a ) {
        for ( b = -1000; b <= 1000; ++b ) {
            n = 0;
	    while(1) {
		
	    	value = n * n + a * n + b;
		if (value <=0) {
		    break;
		}
		uvalue = value;
	    	myInt mint(uvalue);
	    	if (mint.is_prime()) {
		    n++;
	    	} else {
		    break;
		}
	    }
	    if ( n > max_n ) {
		max_n = n;
	        prod_coeff = a * b;
	    }
	    
	}
    }
    EXPECT_EQ(prod_coeff, -59231);
}

TEST (project_euler, problem26)
{
    myInt a(8);
    std::string frac = a.unit_fraction();
    EXPECT_STREQ(frac.c_str(), "125");
    myInt b(7);
    frac = b.unit_fraction();

    int max_period = 0;
    int max_nn = 0;
    for(uint64_t nn = 3; nn < 1000; ++nn) {
        myInt c(nn);
	std::string str = c.unit_fraction(10000);
        int len = str.size() - 1;
	int my_period = 0;
	int i = 1;
	while (i < len/2) {
            std::string substr = str.substr(len - i, i);
	    int n = 2, flag = 0;
            std::string substr2 = str.substr(len - i * n, i);
            if (substr.compare(substr2) == 0) {
	        my_period = i;
		break;
	    } else {
	        i++;
	    }
	}
	if ( my_period > max_period ) {
	    max_period = my_period;
	    max_nn = nn;
	}
    }
    EXPECT_EQ(max_period,982);
    EXPECT_EQ(max_nn,983);
}

TEST (project_euler, problem1)
{

    uint64_t sum = 0;
    for (int i = 1; i <1000; i++) {
       myInt a(i);
       if ( a.is_divisor(3) || a.is_divisor(5) ) {
           sum += a.value();
       }
    }
    EXPECT_EQ(sum, 233168);
}

TEST (project_euler, problem2)
{
    uint64_t a = 1;
    uint64_t b = 2;
    uint64_t c;
    uint64_t sum = 2;
    while (1) {
        c = a + b;
        myInt mi;
        mi.setValue(c);
        if ( mi.is_divisor(2) ) {
            sum += c;
        }

        if ( c >= 4000000 ) break;

        a = b;
        b = c;
    }

    EXPECT_EQ(sum, 4613732);
}

TEST (project_euler, problem3)
{
    EXPECT_TRUE(myInt(19).is_prime());
    myInt a(600851475143);
    EXPECT_NO_THROW(a.calc_prime_divisors());
}

TEST (project_euler, problem4)
{
  int maxint = 0;
  int ii = 0;
  int jj = 0;
  for (int i = 100; i <= 999; i++ ) {
    for (int j = 100; j <= 999; j++) {
        BigInt mint(i*j);
        if (mint.IsPalindromic()) {
            if (i*j > maxint) {
                maxint = i*j;
                ii = i;
                jj = j;
            }
        }
    }
  }

  EXPECT_EQ(maxint, 906609);
}

TEST (project_euler, problem18)
{
    int input[] =
    {75,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     95, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     17, 47, 82,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     18, 35, 87, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     20,  4, 82, 47, 65,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     19,  1, 23, 75,  3, 34,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     88,  2, 77, 73,  7, 63, 67,  0,  0,  0,  0,  0,  0,  0,  0,
     99, 65,  4, 28,  6, 16, 70, 92,  0,  0,  0,  0,  0,  0,  0,
     41, 41, 26, 56, 83, 40, 80, 70, 33,  0,  0,  0,  0,  0,  0,
     41, 48, 72, 33, 47, 32, 37, 16, 94, 29,  0,  0,  0,  0,  0,
     53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14,  0,  0,  0,  0,
     70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57,  0,  0,  0,
     91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48,  0,  0,
     63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31,  0,
      4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23};

     int num_int = sizeof(input) / sizeof(int);
     EXPECT_EQ(num_int, 225);

     std::vector<leaf*> leafs;
     for (int i = 0; i < num_int; i++) {
         leaf* aleaf = new leaf(input[i]);
         leafs.push_back(aleaf);
     }
     EXPECT_EQ(leafs.size(), 225);

     for (int i = 0; i < 14; i++ ) {
         for (int j = 0; j < 14; j++ ) {
             leafs[i*15+j]->setLeft (&leafs[(i+1)*15+j]);
             leafs[i*15+j]->setRight(&leafs[(i+1)*15+j+1]);
         }
     }

     EXPECT_EQ(leafs[0]->max_sum(), 1074);
}

TEST ( project_euler, problem19 ) {
    int dow = 0;
    int dom = 1;
    int mon = 1;
    int year= 1900;
    int sundays = 0;
    myDate md(dow, dom, mon, year);
    while (1) {
        md.nextDay();
        md.todayIs(dow, dom, mon, year);
        if (dow == 6 && dom == 1 && year >= 1901) {
            sundays ++;
        }
        if (dom == 2 && mon == 12 && year == 2000)
            break;
    }

    EXPECT_EQ(sundays, 171);
}

TEST ( project_euler, problem20 ) 
{

    BigInt a = 1;
    for (int i = 1; i <= 100; i++) {
        a = a * i;
    }

    unsigned int digit_sum = a.DigitSum();

    EXPECT_EQ ( digit_sum, 648 );
}

TEST ( project_euler, problem21 )
{
    int div[10001];
    div[0] = 0; // not used
    for(uint64_t i = 1; i <= 10000; i++) {
        myInt b(i);
        div[i] = b.sum_proper_divs();
    }

    // tests from example
    EXPECT_EQ ( div[220], 284 );
    EXPECT_EQ ( div[284], 220 );

    // check if amicable
    int sum = 0;
    int divsum = 0;
    for(uint64_t i = 1; i <= 10000; i++) {
        divsum = div[i];

        // if a == b then skip
        if ( i == divsum ) continue;

        // sum up  
        if ( divsum <= 10000 ) {
            if ( div[divsum] == i ) {
                sum += i;
            }
        }
    }

    // answer
    EXPECT_EQ ( sum, 31626 );
}

TEST ( project_euler, problem22 )
{

    std::vector<std::string> names = ImportRawDataFrom("../Data/p022_names.txt").GetStringsDividedBy("\",");

    std::sort(names.begin(), names.end());
    // remove the first two empty strings
    names.erase(names.begin(), names.begin()+2);
    // ---- calculate scores ----
    uint64_t score = 0;
    long sum = 0;
    int value = 0;
    for (int i = 0; i < names.size(); ++i) {
        // reset name sum
        sum = 0;
        std::string name = names[i];
        for (unsigned j=0; j<name.length(); ++j)
        {
            value = (name.at(j) - 'A' + 1);
            sum += value;
        }
        sum *= (i+1);
        score += sum;
    }

    EXPECT_EQ(score, 871198282);
}

TEST (project_euler, problem23)
{
    myInt a(28);
    EXPECT_EQ(a.is_perfect(), 0);    // 28 is a perfect number
    myInt b(12);
    EXPECT_TRUE(b.is_perfect() > 0); // 12 is an abundant number

//  get all abundant numbers from 1 to 28123
    std::vector<int> perfect_nums;
    for (int i = 1; i <= 28123; ++i) {
        myInt c(i);
        if ( c.is_perfect() > 0 ) // is abundant
            perfect_nums.push_back(i);
    }

    uint64_t sum = 0;
    std::vector<int>::iterator it, it2;
    for (int i = 1; i <= 28123; ++i) {
        int flag = 0;
        for (it = perfect_nums.begin(); it != perfect_nums.end(); ++it ) {
            int cval = i - (*it);
            if (cval <= 0)  break;
            myInt d(cval);
            if (d.is_perfect() > 0) {
                flag = 1;
                break;
            }
        }
        if (flag == 0)
            sum += i;
    }

    EXPECT_EQ(sum, 4179871);
}

TEST (project_euler, problem24)
{
    uint64_t total = 1000000-1;
    for (int i = 9; i >= 0; --i) {
        uint64_t num_perms = largeInt<uint64_t>::factorial(i);
        uint64_t index = largeInt<uint64_t>::get_index(total, num_perms);
        total -= index * num_perms;
        if ( total == 0 ) break;
    }
}

TEST (project_euler, problem25)
{
    typedef BigInt IntType;
    IntType a = 1;
    IntType b = 1;
    IntType c = 1;

    int index = 2;
    
    while (1) {
        index = index + 1;
        c = a + b;
        std::string result = c.GetValueAsStr();

        if (result.length() >= 1000) break;

        a = b;
        b = c;
    }

    EXPECT_EQ(index, 4782);
}

