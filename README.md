# README #

### Brief Intro  ###

This repo contains my progress on solving problems on [ProjectEuler](https://projecteuler.net).

I have currently worked out this many problems:
![alt text](https://projecteuler.net/profile/luqiukai.png)

My current design is to make a library of tools or utilities. All the actual problems then serve as test cases.

### How to build ###

* run cmake

### Dependency ###

Currently the tests depend on googletest, and some algorithms depend on boost.

On a Linux machine, I would recommend installing linuxbrew and get dependency packages from there. For example, you can install boost, cmake from linuxbrew, and then build googletest by yourself.

### Status ###

[![Run Status](https://api.shippable.com/projects/574214992a8192902e20ff51/badge?branch=master)](https://app.shippable.com/projects/574214992a8192902e20ff51)
[![codecov](https://codecov.io/bb/qiukailu/project-euler/branch/master/graph/badge.svg)](https://codecov.io/bb/qiukailu/project-euler)
