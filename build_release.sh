#!/bin/bash -ex

# clean up
rm -rf cmake_build

# build and install
mkdir cmake_build && cd cmake_build && cmake .. && make && cd -
