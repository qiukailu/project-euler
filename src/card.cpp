#include <assert.h>
#include "include/card.h"

namespace Poker {

    Card::Card() {

    }

    Card::Card(std::string card) {
        this->SetValue(card);
    }
    
    Card::~Card() {

    }

    void Card::SetValue(std::string card) {
        m_value = card.at(0);
        m_suit  = card.at(1);
    }

    int Card::GetValueAsInt() {
        if (m_value == '1') {
            return 1;
        } else if (m_value == '2') {
            return 2;
        } else if (m_value == '3') {
            return 3;
        } else if (m_value == '4') {
            return 4;
        } else if (m_value == '5') {
            return 5;
        } else if (m_value == '6') {
            return 6;
        } else if (m_value == '7') {
            return 7;
        } else if (m_value == '8') {
            return 8;
        } else if (m_value == '9') {
            return 9;
        } else if (m_value == 'T') {
            return 10;
        } else if (m_value == 'J') {
            return 11;
        } else if (m_value == 'Q') {
            return 12;
        } else if (m_value == 'K') {
            return 13;
        } else if (m_value == 'A') {
            return 14;
        } else {
            return -1;
        }
    }

    char Card::GetValueAsChar() {
        return m_value;
    }

    char Card::GetSuit() {
        return m_suit;
    }
} // namespace Poker
