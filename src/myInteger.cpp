/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <numeric>
#include <cmath>
#include "include/myInteger.h"
#include "include/largeInt.h"
#include "gmpxx.h"

// TODO: write a function to scan the digits of the integer, return a mapping -- <the repeating digit, an array of indices> 

myInt::myInt()
    : indexedObject()
{
    m_data = 0;
    m_prime_vec.clear();

    m_prime_divisors_calculated = false;
    
}

myInt::myInt(uint64_t in)
    : indexedObject()
{
    setValue(in);
    m_prime_vec.clear();

    m_prime_divisors_calculated = false;
}

myInt::~myInt()
{
	// do nothing
    m_prime_vec.clear();
}

void myInt::setValue(uint64_t in)
{
    m_data = in;
}

uint64_t myInt::value()
{
    return m_data;
}

void myInt::fill_in_primes(int n)
{
    m_prime_vec.clear();
    int value = 2;
    int prime_counter = 0;
    while (1) {
        if (is_prime_num(value)) {
            m_prime_vec.push_back(value);
            prime_counter++;
        }
        if (prime_counter == n) {
            break;
        } else {
            value++;
        }
    }
}

int myInt::num_divisors() 
{
    std::vector<uint64_t> all_divs = largeInt<uint64_t>::get_all_divisors(m_data);
    return all_divs.size();
}

int myInt::sum_of_divisors()
{
    std::vector<uint64_t> all_divs = largeInt<uint64_t>::get_all_divisors(m_data);
    return std::accumulate(all_divs.begin(), all_divs.end(), 0);
}

int myInt::sum_proper_divs()
{
    return (this->sum_of_divisors() - m_data);
}

int myInt::is_perfect() {
    return (this->sum_proper_divs() - m_data);
}

bool myInt::is_prime_num(uint64_t ivalue)
{
    return largeInt<uint64_t>::is_prime_num(ivalue);
}

bool myInt::is_prime() 
{
    return myInt::is_prime_num(m_data);
}

bool myInt::is_divisor(int value) {
    return (m_data % value == 0);
}

void myInt::calc_prime_divisors() {

    m_prime_divisors_calculated = true;

    m_prime_divisors.clear();
    int value = 2;
    int value2 = 0;
    while (1) {
        if (value * value > m_data) {
//	    std::cout << value << std::endl;
//	    std::cout << m_data << std::endl;
	    break;
	}
        if (is_divisor(value)) {
	    value2 = m_data/value;
	    if ( is_prime_num(value) ) {
                m_prime_divisors.push_back(value);
	    }
	    if ( is_prime_num(value2) ) {
	        m_prime_divisors.push_back(value2);
	    }
        }
        value++;
    }
//    std::cout << "All prime divisors -- " << std::endl;
    m_prime_exp.clear();

    for (int i = 0; i < m_prime_divisors.size(); i++) {
        int iexp = 1;
        int ibase = m_prime_divisors[i];
        while ( is_divisor(pow(ibase, iexp)) ) {
            iexp++;
        }
        m_prime_exp.push_back(iexp-1);
    }

    int tnum = 1;
    for (int i=0; i<m_prime_exp.size() ; i++) {
        tnum *= m_prime_exp[i] + 1;
    }
}

std::vector<int> myInt::get_prime_divisors() {
    if ( m_prime_divisors_calculated == false ) {
        this->calc_prime_divisors();
    }

    return m_prime_divisors;
}

std::vector<int> myInt::get_digits()
{
    std::vector<int> digits;
    uint64_t data = this->m_data;
    digits.clear(); 
    int value = data % 10;
    digits.push_back(value);
    data /= 10;

    while (data) {

        value = data % 10;
        digits.push_back(value);
        data /= 10;

    }

    std::reverse(digits.begin(), digits.end());

    return digits;
}

uint64_t myInt::power_sum(int exp)
{
    uint64_t sum = 0;
    std::vector<int> digits = this->get_digits();
    for (int i = 0; i < digits.size(); ++i) {
        sum += std::pow(digits[i], exp);
    }
    return sum;
}

uint64_t myInt::reverse_itself() {

    std::vector<int> mystack = this->get_digits();
    std::reverse(mystack.begin(), mystack.end());
    uint64_t newvalue = largeInt<uint64_t>::get_value_from_digits(mystack);

    return newvalue;
}

std::string myInt::unit_fraction(int prec) {
    long exp;
    int base = 10;
    int n_digits = 0;
    // set defaut precision
    mpf_set_default_prec(prec);
    mpf_t a, b, c;
    mpf_init_set_ui(a, 1);
    mpf_init_set_ui(b, this->value());
    mpf_init(c);
    mpf_div(c, a, b);
    std::string retstr ( mpf_get_str(NULL, &exp, base, n_digits, c) );
    return retstr;
}

bool myInt::is_pandigital() {

    std::vector<int> all_digits = this->get_digits();
    int num_digits = all_digits.size();

    bool retval = true;
    for(int i = 1; i <= num_digits; ++i) {
        if ( std::find(all_digits.begin(), all_digits.end(), i) == all_digits.end() ) {
            // can not find all the digits, thus not pan-digital
            retval = false;
            break;
        }
    }

    return retval;
}

int myInt::get_num_digits() {
    std::vector<int> all_digits = this->get_digits();
    return all_digits.size();
}

std::map<int, std::vector<int>> myInt::get_repeating_digits_map() {

    std::map<int, std::vector<int>> repeating_digits_map;

    std::vector<int> all_digits = this->get_digits();
    for (int i = 0; i <= 9; i++) {
        std::vector<int> indices;
        indices.clear();
        for(int j = 0; j < all_digits.size(); j++) {

            if (all_digits[j] == i) {
                indices.push_back(j);
            }
        
        }

        repeating_digits_map[i] = indices;
    }

    return repeating_digits_map;
}

