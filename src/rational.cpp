#include <sstream>
#include <vector>
#include <assert.h>

#include "include/rational.h"
#include "include/largeInt.h"

Rational::Rational(std::string in_str) {

    std::vector<std::string> values;
    std::istringstream f(in_str);
    std::string s;
    while (getline(f, s, '/')) {
        values.push_back(s);
    }

    assert(values.size() == 1 || values.size() == 2);

    mp_above = new BigInt(values[0]);

    if (values.size() == 2) {
        mp_below = new BigInt(values[1]);
    } else {
        mp_below = new BigInt("1");
    }
}

Rational::Rational(BigInt in_above, BigInt in_below) {
    mp_above = new BigInt(in_above);
    mp_below = new BigInt(in_below);
}

Rational::~Rational() {
    delete mp_above;
    delete mp_below;
}

Rational::Rational(const Rational & other_rational) {
    mp_above = new BigInt(*(other_rational.mp_above));
    mp_below = new BigInt(*(other_rational.mp_below));
}

Rational & Rational::operator = (const Rational & other_rational) {
    mp_above = new BigInt(*(other_rational.mp_above));
    mp_below = new BigInt(*(other_rational.mp_below));
    return *this;
}

BigInt Rational::GetAbove() const {
    return *(mp_above);
}

BigInt Rational::GetBelow() const {
    return *(mp_below);
}

// private functions //

// friends //
Rational operator + (const Rational & lhs, const Rational & rhs) {
    BigInt new_below = lhs.GetBelow() * rhs.GetBelow();
    BigInt new_above = lhs.GetAbove() * rhs.GetBelow() + lhs.GetBelow() * rhs.GetAbove();

    BigInt bigcd = largeInt<BigInt>::get_gcd(new_above, new_below);

    new_below = new_below / bigcd;
    new_above = new_above / bigcd;

    return Rational(new_above, new_below);
}

Rational operator / (const Rational & lhs, const Rational & rhs) {
    BigInt new_above = lhs.GetAbove() * rhs.GetBelow();
    BigInt new_below = lhs.GetBelow() * rhs.GetAbove();

    BigInt bigcd = largeInt<BigInt>::get_gcd(new_above, new_below);

    new_below = new_below / bigcd;
    new_above = new_above / bigcd;

    return Rational(new_above, new_below);
}
