/*
The MIT License (MIT)
Copyright (c) 2016 Qiukai Lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
//
//  mytree.cpp
//  
//
//  Created by Qiukai Lu on 8/4/16.
//
//
#include <cstdio>
#include <algorithm>

#include "include/mytree.h"

leaf::leaf(int i)
{
    m_value = i;
    m_left  = NULL;
    m_right = NULL;
}

leaf::leaf(int i, leaf** left, leaf** right)
{
    setValue(i);
    setLeft(left);
    setRight(right);
}

void leaf::setLeft(leaf ** p_leaf) {
    m_left = *p_leaf;
}

void leaf::setRight(leaf ** p_leaf ) {
    m_right = *p_leaf;
}

void leaf::setValue(int i_value){
    m_value = i_value;
}

int leaf::max_sum() {
    if (getLeft() == NULL && getRight() == NULL) {
      return getValue();
    }

    int sum = getValue() + std::max(m_left->max_sum(), m_right->max_sum());
    return sum;
}

leaf* leaf::getLeft() {
    return m_left;
}

leaf* leaf::getRight() {
    return m_right;
}

int leaf::getValue() {
    return m_value;
}
