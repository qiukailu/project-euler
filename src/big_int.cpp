#include <algorithm>

#include "include/big_int.h"
// =============================================================================
// public member functions
// =============================================================================

BigInt::BigInt (signed long int in_si_data) {
    this->_Initialize();
    mpz_set_si(m_data, in_si_data);
}

BigInt::BigInt (int in_data) {
    this->_Initialize();
    signed long int si_data = (signed long int)in_data;
    mpz_set_si(m_data, si_data);
}

BigInt::BigInt (mpz_t in_data) {
    this->_Initialize();
    mpz_set(m_data, in_data);
}

BigInt::BigInt (std::string in_string) {
    this->_Initialize();
    mpz_set_str(m_data, in_string.c_str(), 10);
}

BigInt::~BigInt () {

    mpz_clear(m_data);

}

BigInt::BigInt(const BigInt & other_big_int) {
    this->_Initialize();
    mpz_set(m_data, other_big_int.m_data);
}

BigInt & BigInt::operator=(const BigInt & other_big_int) {
    mpz_set(m_data, other_big_int.m_data);
    return *this;
}

bool BigInt::operator==(const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval == 0);

}

bool BigInt::operator!=(const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval != 0);

}

bool BigInt::operator > (const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval > 0);

}

bool BigInt::operator >= (const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval > 0 || retval ==0);

}

bool BigInt::operator < (const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval < 0);

}

bool BigInt::operator <= (const BigInt & other_big_int) {

    int retval = mpz_cmp(m_data, other_big_int.m_data);
    return (retval < 0 || retval ==0);

}


std::string BigInt::GetValueAsStr() {
    std::string value(mpz_get_str(NULL, 10, m_data));
    return value;
}

bool BigInt::IsPalindromic() {
    std::string value = this->GetValueAsStr();
    std::string copy(value);
    std::reverse(copy.begin(), copy.end());
    return (value == copy);
}

BigInt BigInt::Reverse() {
    std::string value = this->GetValueAsStr();
    std::string copy(value);
    std::reverse(copy.begin(), copy.end());
    BigInt retval(copy);
    return retval;
}

bool BigInt::IsLychrel(unsigned int & num_iter) {
    return this->IsLychrel(50, num_iter);
}

bool BigInt::IsLychrel(unsigned int max_iter, unsigned int & num_iter) {
    BigInt copy = (*this);
    // get reverse big int
    BigInt rev = this->Reverse();
    BigInt sum = copy + rev;
    num_iter = 1;
    if (sum.IsPalindromic()) {
        return false;
    } else {
    
        while (1) {
            copy = sum;
            rev  = sum.Reverse();
            sum  = copy + rev;
            num_iter = num_iter + 1;
            if (sum.IsPalindromic()) {
                return false;
            }

            if (num_iter >= max_iter) {
                return true;
            }
        }
    }
}

unsigned int BigInt::DigitSum() {
    std::string result = this->GetValueAsStr();
    unsigned int digit_sum = 0;
    int value;
    for ( std::string::iterator it = result.begin(); it!=result.end(); ++it ) {
        value = *it - '0';
        digit_sum += value;
    }
    return digit_sum;
}
            
unsigned int BigInt::NumDigits() {
    std::string result = this->GetValueAsStr();
    return result.size();
}


BigInt BigInt::ConcatWith(BigInt & b) {
    std::string result1 = this->GetValueAsStr();
    std::string result2 = b.GetValueAsStr();
    std::string result3 = result1 + result2;
    return BigInt(result3);
}

// =============================================================================
// private member functions
// =============================================================================//


void BigInt::_Initialize() {

    mpz_init(m_data);

}

// ==================
// friends
// ==================
BigInt operator + (const BigInt & lhs, const BigInt & rhs) {
    mpz_t result;
    mpz_init(result);
    mpz_add(result, lhs.m_data, rhs.m_data);
    return BigInt(result);
}

BigInt operator - (const BigInt & lhs, const BigInt & rhs) {
    mpz_t result;
    mpz_init(result);
    mpz_sub(result, lhs.m_data, rhs.m_data);
    return BigInt(result);
}

BigInt operator * (const BigInt & lhs, const BigInt & rhs) {
    mpz_t result;
    mpz_init(result);
    mpz_mul(result, lhs.m_data, rhs.m_data);
    return BigInt(result);
}

BigInt operator / (const BigInt & lhs, const BigInt & rhs) {
    mpz_t result;
    mpz_init(result);
    mpz_tdiv_q(result, lhs.m_data, rhs.m_data);
    return BigInt(result);
}

BigInt operator % (const BigInt & lhs, const BigInt & rhs) {
    mpz_t result;
    mpz_init(result);
    mpz_mod(result, lhs.m_data, rhs.m_data);
    return BigInt(result);
}

BigInt operator ^ (const BigInt & lhs, const unsigned long int exp) {
    mpz_t result;
    mpz_init(result);
    mpz_pow_ui(result, lhs.m_data, exp);
    return BigInt(result);
}
