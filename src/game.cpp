#include <string>
#include <vector>
#include <assert.h>

#include <boost/algorithm/string.hpp>

#include "include/card.h"
#include "include/hand.h"
#include "include/game.h"

namespace Poker {

    Game::Game(std::string deck) {
        using namespace boost::algorithm;
        std::vector<std::string> retval;
        split(retval, deck, is_any_of(" "), token_compress_on);    
        assert(retval.size() == 10);

        std::vector<Card *> cards;
        for (int i = 0; i < retval.size(); i++) {
            Card * c = new Card(retval[i]);
            cards.push_back(c);
        }

        m_player1 = new Hand();
        for (int i = 0; i < 5; i++)
            m_player1->AssignCard(cards[i]);

        m_player2 = new Hand();
        for (int i = 5; i < 10; i++)
            m_player2->AssignCard(cards[i]);

    }

    Game::~Game() {
    }

    int Game::CompareRanks() {
            int rank1 = m_player1->GetRank();
            int rank2 = m_player2->GetRank();

            if (rank1 == rank2) {

                return 0;

            } else if (rank1 > rank2) {
                return 1;
            } else {

                return -1;

            }
    }

    bool Game::Player1Wins() {
            int retval = this->CompareRanks();

            // clear winner with rank difference
            if (retval == 1) return true;
            if (retval == -1) return false;

            // same rank, lets do further comparison
            std::vector <int> p1_weighted_values = m_player1->GetWeightedCardValues();
            std::vector <int> p2_weighted_values = m_player2->GetWeightedCardValues();

            std::sort(p1_weighted_values.begin(), p1_weighted_values.end());
            std::sort(p2_weighted_values.begin(), p2_weighted_values.end());

            for (int j = p1_weighted_values.size() - 1; j >= 0; j--) {
                if (p1_weighted_values[j] != p2_weighted_values[j]) {
                    return (p1_weighted_values[j] > p2_weighted_values[j]);
                }
            }

            // should never reach this part
            return false;
    }

} // namespace Poker
