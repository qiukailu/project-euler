#include <algorithm>
#include <assert.h>
#include "include/card.h"
#include "include/hand.h"

namespace Poker {

    Hand::Hand() {
        m_p_cards.clear();
    }

    Hand::~Hand() {
        m_p_cards.clear();
    }

    void Hand::AssignCard(Card * pcard) {
        m_p_cards.push_back(pcard);
        m_weighted_card_values.push_back(pcard->GetValueAsInt());
        assert(m_p_cards.size() >= 0 && m_p_cards.size() <= 5);
    }

    int Hand::GetNumberOfCardsByValue(char cvalue) {
        int counter = 0;
        for (int i = 0; i < m_p_cards.size(); i++) {
            char card_value = m_p_cards[i]->GetValueAsChar();
            if (card_value == cvalue) {
                counter = counter + 1;
            }
        }
        return counter;
    }

    int Hand::GetNumberOfCardsBySuit(char csuit) {
        int counter = 0;
        for (int i = 0; i < m_p_cards.size(); i++) {
            char card_suit = m_p_cards[i]->GetSuit();
            if (card_suit == csuit) {
                counter = counter + 1;
            }
        }
        return counter;
    }

    bool Hand::IsRoyalFlush() {

        bool AllInSameSuit = this->_AllInSameSuit();

        bool HasTen = (GetNumberOfCardsByValue('T') == 1);
        bool HasJ   = (GetNumberOfCardsByValue('J') == 1);
        bool HasQ   = (GetNumberOfCardsByValue('Q') == 1);
        bool HasK   = (GetNumberOfCardsByValue('K') == 1);
        bool HasA   = (GetNumberOfCardsByValue('A') == 1);

        return (AllInSameSuit && HasTen && HasJ && HasQ && HasK && HasA);
    }

    bool Hand::IsStraightFlush() {
        return (this->_AllInSameSuit() && this->_ConsecutiveCards());
    }
    
    bool Hand::IsFourOfAKind() {
        std::vector<char> card_values{'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
        bool found = false;
        char found_char = 'x';
        for (auto i : card_values) {
            if (4 == GetNumberOfCardsByValue(i)) {
                found = true;
                found_char = i;
                break;
            }
        }

        if (found) { // weight card values
            for (int j = 0; j < m_p_cards.size(); j++) {
                if (found_char == m_p_cards[j]->GetValueAsChar()) {
                    m_weighted_card_values[j] *= 1000000;
                }
            }
        }

        return found;
    }

    bool Hand::IsFullHouse() {
        std::vector<char> card_values{'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
        bool found_3 = false;
        bool found_2 = false;

        char found_3_char = 'x';
        char found_2_char = 'x';

        for (auto i : card_values) {
            int number_of_i_cards = GetNumberOfCardsByValue(i);
            if (3 == number_of_i_cards) {
                found_3 = true;
                found_3_char = i;
            } else if (2 == number_of_i_cards) {
                found_2 = true;
                found_2_char = i;
            }
        }

        if (found_3 && found_2) { // weight card values

            for (int j = 0; j < m_p_cards.size(); j++) {
                if (found_3_char == m_p_cards[j]->GetValueAsChar()) {
                    m_weighted_card_values[j] *= 10000;
                } else if (found_2_char == m_p_cards[j]->GetValueAsChar()) {
                    m_weighted_card_values[j] *= 100;
                }
            }


        }

        return (found_3 && found_2);        
    }

    bool Hand::IsFlush() {
        // no weight in this case
        return this->_AllInSameSuit();
    }

    bool Hand::IsStraight() {
        // no need to weight
        return this->_ConsecutiveCards();
    }

    bool Hand::IsThreeOfAKind() {
        std::vector<char> card_values{'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
        bool found_3 = false;
        bool found_2 = false;

        char found_3_char = 'x';

        for (auto i : card_values) {
            int number_of_i_cards = GetNumberOfCardsByValue(i);
            if (3 == number_of_i_cards) {
                found_3 = true;
                found_3_char = i;
            } else if (2 == number_of_i_cards) {
                found_2 = true;
            }
        }

        if (found_3 && !(found_2)) { // weight card values

            for (int j = 0; j < m_p_cards.size(); j++) {
                if (found_3_char == m_p_cards[j]->GetValueAsChar()) {
                    m_weighted_card_values[j] *= 10000;
                }
            }
        }

        return (found_3 && !(found_2));        
    }

    bool Hand::HasTwoPairs() {
        std::vector<char> pair_values;

        if (2 == this->_GetNumberOfPairs(pair_values)) {

            for (int j = 0; j < m_p_cards.size(); j++) {
                char card_value = m_p_cards[j]->GetValueAsChar();
                if ((card_value == pair_values[0]) || (card_value == pair_values[1])){
                    m_weighted_card_values[j] *= 100;
                }
            }

            return true;
        }

        return false;
    }

    bool Hand::HasOnePair() {
        std::vector<char> pair_values;

        if (1 == this->_GetNumberOfPairs(pair_values)) {

            for (int j = 0; j < m_p_cards.size(); j++) {
                char card_value = m_p_cards[j]->GetValueAsChar();
                if (card_value == pair_values[0]){
                    m_weighted_card_values[j] *= 100;
                }
            }

            return true;
        }

        return false;
    }

    int Hand::GetHighestValueAsInt() {
        int highest_value = -1;
        for (int i = 0; i < m_p_cards.size(); i++) {
            int ivalue = m_p_cards[i]->GetValueAsInt();
            highest_value = std::max(ivalue, highest_value);
        }
        return highest_value;
    }

    int Hand::GetRank() {
        if (IsRoyalFlush()) {
            return ROYAL_FLUSH;
        }

        if (IsStraightFlush()) {
            return STRAIGHT_FLUSH;
        }

        if (IsFourOfAKind()) {
            return FOUR_OF_A_KIND;
        }

        if (IsFullHouse()) {
            return FULL_HOUSE;
        }

        if (IsFlush()) {
            return FLUSH;
        }

        if (IsStraight()) {
            return STRAIGHT;
        }

        if (IsThreeOfAKind()) {
            return THREE_OF_A_KIND;
        }

        if (HasTwoPairs()) {
            return TWO_PAIRS;
        }

        if (HasOnePair()) {
            return ONE_PAIR;
        }

        return PLAIN;
    }

    std::vector<int> Hand::GetWeightedCardValues() {
        return m_weighted_card_values;
    }

    //////
    bool Hand::_AllInSameSuit() {

        int number_of_spades   = this->GetNumberOfCardsBySuit('S');
        int number_of_hearts   = this->GetNumberOfCardsBySuit('H');
        int number_of_clubs    = this->GetNumberOfCardsBySuit('C');
        int number_of_diamonds = this->GetNumberOfCardsBySuit('D');

        return ((number_of_spades == 5) || (number_of_hearts == 5) || (number_of_clubs == 5) || (number_of_diamonds == 5));

    }

    bool Hand::_ConsecutiveCards() {
        std::vector<int> card_values;
        for (int i = 0; i < m_p_cards.size(); i++) {
            int ivalue = m_p_cards[i]->GetValueAsInt();
            card_values.push_back(ivalue);
        }

        std::sort(card_values.begin(), card_values.end());

        return (((card_values[4] - card_values[3]) == 1) && ((card_values[3] - card_values[2]) == 1) && ((card_values[2] - card_values[1]) == 1) && ((card_values[1] - card_values[0]) == 1));

    }

    int Hand::_GetNumberOfPairs(std::vector<char> & pair_values) {
        pair_values.clear();

        std::vector<char> card_values{'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
        int counter = 0;

        for (auto i : card_values) {
            int number_of_i_cards = GetNumberOfCardsByValue(i);
            if (2 == number_of_i_cards) {
                counter = counter + 1;
                pair_values.push_back(i);
            }
        }

        assert(counter == pair_values.size());
        return counter;
    }

} // end namespace Poker
