#include "include/standalone.h"

int make_money(int total, std::stack<int> list_of_denom)
{
    if (list_of_denom.size() == 1) return 1;

    int sum = 0;
    int max_denom = list_of_denom.top();
//    std::cout << "max_denom: " << max_denom << std::endl;
    list_of_denom.pop();
    std::stack<int> new_list = list_of_denom;
    if (total % max_denom == 0) sum += 1;
    for (int i = 0; ; ++i) {
        int rest_total = total - i * max_denom;
        if (rest_total <= 0) break;
        sum += make_money(rest_total, new_list);
//        std::cout << "sum: " << sum << std::endl;
    }
    return sum;

}
